<?php $pdo=new PDO('mysql:host=localhost;dbname=simpeg;','root','');
$qry=$pdo->prepare("SELECT * FROM tr_units where lvl=0");
$qry->execute();
$data=$qry->fetchAll(PDO::FETCH_ASSOC);

//echo json_encode($data); ?>
<script type="text/javascript">
	$(function(){
	var data = [
	<?php foreach ($data as $dta) {
		$qr_c1=$pdo->prepare("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
		$qr_c1->execute();
		$cek1=$qr_c1->fetchAll(PDO::FETCH_ASSOC); ?>
		//data level 1
		{
		id: <?php echo $dta['id']; ?>,
		text: "<?php echo $dta['name']; ?>",
		//data level 2 jika ada
		<?php if ($cek1>0) {?>
			children: [
			<?php $qry_2=$pdo->prepare("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
			$qry_2->execute();
			foreach ($qry_2->fetchAll(PDO::FETCH_ASSOC) as $dta2) {
				$qr_c2=$pdo->prepare("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
				$qr_c2->execute();
				$cek2=$qr_c2->fetchAll(PDO::FETCH_ASSOC); ?>
			{
				id: <?php echo $dta2['id']; ?>,
				text: "<?php echo $dta2['name']; ?>",
				//data level 3 jika ada
				<?php if ($cek2>0) { ?>
				children: [
				<?php $qry_3=$pdo->prepare("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
				$qry_3->execute();
				foreach ($qry_3->fetchAll(PDO::FETCH_ASSOC) as $dta3) { ?>
				{
					id: <?php echo $dta3['id']; ?>,
					text: "<?php echo $dta3['name']; ?>",
				},
				<?php } ?>
				]
				<?php }else{} ?>
			},
			<?php } ?>
			]
			<?php } else{} ?>
		},
		<?php } ?>];
		console.log(data);
	$('#cc').combotree('loadData', data);
});
</script>