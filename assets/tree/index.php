<?php $pdo= new PDO('mysql:host=localhost;dbname=simpeg;','root',''); ?>
<html>
<head>
	<title></title>
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
        crossorigin="anonymous">
	</script>
	<script src="comboTreePlugin.js"></script>
	<script src="icontains.js"></script>
</head>
<body>
<input type="text" id="example" placeholder="Select">
</body>
<script type="text/javascript">
	var myData = [
    {
    id: 0,
    title: 'choice 1  '
}, {
    id: 1,
    title: 'choice 2',
    subs: [
        {
            id: 10,
            title: 'choice 2 1'
        }, {
            id: 11,
            title: 'choice 2 2'
        }, {
            id: 12,
            title: 'choice 2 3'
        }
    ]
}, {
    id: 2,
    title: 'choice 3'
}, {
    id: 3,
    title: 'choice 4'
}, {
    id: 4,
    title: 'choice 5'
}, {
    id: 5,
    title: 'choice 6',
    subs: [
        {
            id: 50,
            title: 'choice 6 1'
        }, {
            id: 51,
            title: 'choice 6 2',
            subs: [
                {
                    id: 510,
                    title: 'choice 6 2 1'
                }, {
                    id: 511,
                    title: 'choice 6 2 2'
                }, {
                    id: 512,
                    title: 'choice 6 2 3'
                }
            ]
        }
    ]
}, {
    id: 6,
    title: 'choice 7'
}
    // more data here
];

$('#example').comboTree({
  source : myData
});
</script>
</html>