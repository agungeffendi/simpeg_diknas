<?php
/**
 * 
 */
class Tm_kgb_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_kgb()
	{
		$this->db->select('*')
		->from('tm_peg_kgb')
		->where('i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function fetch()
	{
		$query = $this->db->get_where('tm_kgb_m', array('id' => $this->input->post['id']));
		return $query->row_array();
	}

	public function insert($img)
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'sk_lama' => $this->input->post('sk_now'),
			'tgl_sk_lama' => date("Ymd",strtotime($this->input->post('tgl_sk_now'))),
			'tgl_berlaku_lama' => date("Ymd",strtotime($this->input->post('rea_sk_now'))),
			'ttd_lama' => $this->input->post('ttd_sk_now'),
			'gapok_lama' => $this->input->post('sal_now'),
			'sk_baru' => $this->input->post('sk'),
			'tgl_sk_baru' => date("Ymd",strtotime($this->input->post('tgl_sk'))),
			'tgl_berlaku_baru' => date("Ymd",strtotime($this->input->post('rea_sk'))),
			'ttd_baru' => $this->input->post('ttd_sk'),
			'gapok_baru' => $this->input->post('sal'),
			'scan_sk' => $img
		);
		return $this->db->insert('tm_peg_kgb', $data);
	}
}