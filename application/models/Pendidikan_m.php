<?php
/**
 * 
 */
class Pendidikan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pendidikan($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_pendidikan');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_pendidikan', array('i_pdk' => $id));
		return $query->row_array();
	}
}