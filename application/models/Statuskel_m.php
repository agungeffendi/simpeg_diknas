<?php
/**
 * 
 */
class Statuskel_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_statuskel($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_statuskel');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_statuskel', array('i_statuskel' => $id));
		return $query->row_array();
	}
}