<?php
/**
 * 
 */
class Tm_keluarga_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_keluarga()
	{
		$this->db->select('a.*, b.n_statuskel')
		->from('tm_peg_keluarga a')
		->join('tr_statuskel b','a.i_statuskel=b.i_statuskel')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function get_edit()
	{
		$this->db->select('a.*, b.n_statuskel')
		->from('tm_peg_keluarga a')
		->join('tr_statuskel b','a.i_statuskel=b.i_statuskel')
		->where('a.i_peg_keluarga', $this->input->post('id'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'n_peg_keluarga' => $this->input->post('nama'),
			'i_statuskel' => $this->input->post('status'),
			'd_peg_keluarga_lahir' => date("Y-m-d",strtotime($this->input->post('ttl'))),
			'n_peg_keluarga_pekerjaan' => $this->input->post('pekerjaan'),
			'e_peg_keluarga' => $this->input->post('keterangan')
		);
		return $this->db->insert('tm_peg_keluarga', $data);
	}

	public function update()
	{
		$data = array(
			'i_peg_keluarga' => $this->input->post('id'),
			'n_peg_keluarga' => $this->input->post('nama'),
			'i_statuskel' => $this->input->post('status'),
			'd_peg_keluarga_lahir' => date("Y-m-d",strtotime($this->input->post('ttl'))),
			'n_peg_keluarga_pekerjaan' => $this->input->post('pekerjaan'),
			'e_peg_keluarga' => $this->input->post('keterangan')
		);
		$this->db->where('i_peg_keluarga',$this->input->post('id'));
		$query=$this->db->update('tm_peg_keluarga',$data);

		return $query;
	}

	public function delete()
	{
		$this->db->where('i_peg_keluarga', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_keluarga');

		return $query;
	}
}