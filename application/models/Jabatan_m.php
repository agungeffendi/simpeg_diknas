<?php
/**
 * 
 */
class Jabatan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_jabatan($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_jabatan');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_jabatan', array('i_jabatan' => $id));
		return $query->row_array();
	}
}