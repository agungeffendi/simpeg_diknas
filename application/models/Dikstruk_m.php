<?php
/**
 * 
 */
class Dikstruk_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_dikstruk($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_diklatstrukt');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_diklatstrukt', array('i_diklatstruk' => $id));
		return $query->row_array();
	}
}