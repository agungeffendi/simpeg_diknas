<?php
/**
 * 
 */
class Status_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_status($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_statuspeg');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_statuspeg', array('i_statuspeg' => $id));
		return $query->row_array();
	}
}