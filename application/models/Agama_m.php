<?php
/**
 * 
 */
class Agama_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_agama($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_agama');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_agama', array('i_agama' => $id));
		return $query->row_array();
	}
}