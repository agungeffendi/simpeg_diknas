<?php
/**
 * 
 */
class Tm_diktek_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_diklat()
	{
		$this->db->select('a.*, b.n_diklattek')
		->from('tm_peg_diklattek a')
		->join('tr_diklattek b','a.i_diklattek=b.i_diklattek')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function insert()
	{
		$jam=24*$this->input->post('lamanya');
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'i_diklattek' => $this->input->post('jenis'),
			'e_peg_diklattek' => $this->input->post('lokasi'),
			'v_peg_diklattek_durasijam' => $jam,
			'c_peg_diklattek_tahun' => $this->input->post('tahun'),
			'e_diklattek_freetext' => $this->input->post('keterangan')
		);
		return $this->db->insert('tm_peg_diklattek', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_diklattek', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_diklattek');

		return $query;
	}
}