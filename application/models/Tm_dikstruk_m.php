<?php
/**
 * 
 */
class Tm_dikstruk_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_diklat()
	{
		$this->db->select('a.*, b.n_diklatstruk')
		->from('tm_peg_diklatstruk a')
		->join('tr_diklatstrukt b','a.i_diklatstruk=b.i_diklatstruk')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}
	public function insert()
	{
		$jam=24*$this->input->post('lamanya');
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'i_diklatstruk' => $this->input->post('jenis'),
			'a_peg_diklatstruk_lokasi' => $this->input->post('lokasi'),
			'v_peg_diklatstruk_durasijam' => $jam,
			'c_peg_diklatstruk_tahun' => $this->input->post('tahun'),
			'e_peg_diklatstruk_freetext' => $this->input->post('keterangan')
		);
		return $this->db->insert('tm_peg_diklatstruk', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_diklatstruk', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_diklatstruk');

		return $query;
	}
}