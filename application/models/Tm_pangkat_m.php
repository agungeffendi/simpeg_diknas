<?php
/**
 * 
 */
class Tm_pangkat_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pangkat()
	{
		$this->db->select('a.*, b.n_pangkat')
		->from('tm_peg_pangkat a')
		->join('tr_pangkat b','a.i_pangkat=b.i_pangkat')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function get_edit()
	{
		$this->db->select('a.*, b.n_pangkat')
		->from('tm_peg_pangkat a')
		->join('tr_pangkat b','a.i_pangkat=b.i_pangkat')
		->where('a.i_peg_pangkat', $this->input->post('id'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'c_peg_pangkat_sk' => $this->input->post('no_sk'),
			'd_peg_pangkat_sk' => date("Y-m-d",strtotime($this->input->post('tsk'))),
			'd_peg_pangkat_tmt' => date("Y-m-d",strtotime($this->input->post('tmt'))),
			'i_pangkat' => $this->input->post('pangkat'),
			'n_peg_pangkat_tdtgn' => $this->input->post('ttd')
		);
		//echo $this->db->last_query(); die();
		return $this->db->insert('tm_peg_pangkat', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_pangkat', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_pangkat');

		return $query;
	}
}