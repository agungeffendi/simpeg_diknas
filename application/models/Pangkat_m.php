<?php
/**
 * 
 */
class Pangkat_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pangkat($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_pangkat');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_pangkat', array('i_pangkat' => $id));
		return $query->row_array();
	}
}