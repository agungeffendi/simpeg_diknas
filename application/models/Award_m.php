<?php
/**
 * 
 */
class Award_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_award($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_penghargaan');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_penghargaan', array('i_penghargaan' => $id));
		return $query->row_array();
	}
}