<?php
/**
 * 
 */
class Tm_pendidikan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pendidikan()
	{
		$this->db->select('a.*, b.n_pdk, c.n_jurusan')
		->from('tm_peg_pdkformal a')
		->join('tr_pendidikan b','a.i_pdk=b.i_pdk')
		->join('tr_jurusan c','a.i_jurusan=c.i_jurusan')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function get_edit()
	{
		$this->db->select('a.*, b.n_pdk, c.n_jurusan')
		->from('tm_peg_pdkformal a')
		->join('tr_pendidikan b','a.i_pdk=b.i_pdk')
		->join('tr_jurusan c','a.i_jurusan=c.i_jurusan')
		->where('a.i_peg_pdkformal', $this->input->post('id'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'i_pdk' => $this->input->post('jenjang'),
			'i_jurusan' => $this->input->post('jurusan'),
			'a_peg_pdkformal_lokasi' => $this->input->post('instansi'),
			'c_peg_pdkformal_lulus' => date("Y-m-d",strtotime($this->input->post('tlulus')))
		);
		return $this->db->insert('tm_peg_pdkformal', $data);
	}
	public function delete()
	{
		$this->db->where('i_peg_pdkformal', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_pdkformal');

		return $query;
	}
}