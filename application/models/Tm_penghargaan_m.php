<?php
/**
 * 
 */
class Tm_penghargaan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_penghargaan()
	{
		$this->db->select('a.*, b.n_penghargaan')
		->from('tm_peg_penghargaan a')
		->join('tr_penghargaan b','a.i_penghargaan=b.i_penghargaan')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'i_penghargaan' => $this->input->post('award'),
			'c_peg_penghargaan_tahun' => $this->input->post('taward'),
			'e_peg_penghargaan' => $this->input->post('keterangan')
		);
		return $this->db->insert('tm_peg_penghargaan', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_penghargaan', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_penghargaan');

		return $query;
	}
}