<?php
/**
 * 
 */
class Hukuman_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_hukuman($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_hukuman');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_hukuman', array('i_hukuman' => $id));
		return $query->row_array();
	}
}