<?php
/**
 * 
 */
class User_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_user($nip = FALSE)
	{
		if ($nip === FALSE) {
			$query = $this->db->get('user');
			return $query->result_array();
		}
		$query = $this->db->get_where('user', array('username' => $nip));
		return $query->row_array();
	}

	public function create_user()
	{
		$data = array(
			'username'=>$this->input->post('username'),
			'email'=>$this->input->post('mail'),
			'password_hash'=>sha1($this->input->post('password')),
			'i_unit'=>$this->input->post('unit')

		);
		return $this->db->insert('user', $data);
	}

	public function update_user()
	{
		$data = array(
			'email'=>$this->input->post('mail'),
			'password_hash'=>sha1($this->input->post('password'))
		);
		$this->db->where('id',$this->input->post('uid'));
		$this->db->update('user',$data);
	}

	public function delete()
	{
		$this->db->where('id', $this->input->post('uid'));
		$this->db->delete('user');
	}

	public function auth($table,$where){
		return $this->db->get_where($table,$where);
	}

	public function ajax_call($postdata)
	{
		$response = array();
 
 		if($postdata['username']){
	 
	   // Select record
	   $this->db->select('*');
	   $this->db->where('username', $postdata['username']);
	   $q = $this->db->get('user');
	   $response = $q->result_array();
	  }
	  return $response;
	}

}