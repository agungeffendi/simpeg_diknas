<?php
/**
 * 
 */
class Tm_pekerjaan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pekerjaan()
	{
		$this->db->select('a.*, b.n_jabatan')
		->from('tm_peg_pekerjaan a')
		->join('tr_jabatan b','a.i_jabatan=b.i_jabatan')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function get_edit()
	{
		$this->db->select('a.*, b.n_jabatan')
		->from('tm_peg_pekerjaan a')
		->join('tr_jabatan b','a.i_jabatan=b.i_jabatan')
		->where('a.i_peg_kerja', $this->input->post('id'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'c_peg_kerja_awal' => $this->input->post('tawal'),
			'c_peg_kerja_akhir' => $this->input->post('takhir'),
			'i_jabatan' => $this->input->post('jabatan'),
			'i_unit' => $this->input->post('unit'),
			'c_peg_kerja_sk' => $this->input->post('no_sk'),
			'e_peg_kerja_freetext' => $this->input->post('keterangan')
		);
		//echo $this->db->last_query(); die();
		return $this->db->insert('tm_peg_pekerjaan', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_kerja', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_pekerjaan');

		return $query;
	}
}