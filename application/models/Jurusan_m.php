<?php
/**
 * 
 */
class Jurusan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_jurusan($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_jurusan');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_jurusan', array('i_jurusan' => $id));
		return $query->row_array();
	}
}