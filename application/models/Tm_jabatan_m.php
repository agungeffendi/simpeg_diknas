<?php
/**
 * 
 */
class Tm_jabatan_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_jabatan()
	{
		$this->db->select('a.*, b.n_jabatan')
		->from('tm_peg_jabatan a')
		->join('tr_jabatan b','a.i_jabatan=b.i_jabatan')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'd_peg_jabat_tmt' => date("Y-m-d",strtotime($this->input->post('tmt'))),
			'c_peg_jabat_sk' => $this->input->post('no_sk'),
			'i_unit' => '0',
			'i_jabatan' => $this->input->post('jabatan'),
			'n_peg_jabat_tdtgn' => $this->input->post('ttd'),
			'e_peg_jabat_freetext' => $this->input->post('keterangan')
		);
		//echo $this->db->last_query(); die();
		return $this->db->insert('tm_peg_jabatan', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_jabat', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_jabatan');

		return $query;
	}
}