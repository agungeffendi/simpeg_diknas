<?php
/**
 * 
 */
class Unit_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_unit($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_units');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_units', array('id' => $id));
		return $query->row_array();
	}

	public function get_unit_id($id)
	{
		$query = $this->db->get_where('tr_units', array('id' => $id));
		$data=$query->row_array();
		$geteselon= $this->db->get_where('tr_units', array('id' => $data['root']));
		return $geteselon->row_array();
	}
}