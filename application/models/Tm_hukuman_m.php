<?php
/**
 * 
 */
class Tm_hukuman_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_hukuman()
	{
		$this->db->select('a.*, b.n_hukuman')
		->from('tm_peg_hukuman a')
		->join('tr_hukuman b','a.i_hukuman=b.i_hukuman')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}
	public function insert()
	{
		$data = array(
			'i_peg' => $this->input->post('i_peg'),
			'i_hukuman' => $this->input->post('jenis'),
			'd_peg_hukuman_sk' => date("Y-m-d",strtotime($this->input->post('tgl_sk'))),
			'i_peg_hukuman_sk' => $this->input->post('no_sk'),
			'v_peg_hukuman_durasi' => $this->input->post('lama_sk'),
			'd_peg_hukuman_tmt' => date("Y-m-d",strtotime($this->input->post('tmt'))),
			'c_peg_hukuman_refpp' => $this->input->post('ref'),
			'e_peg_hukuman_alasan' => $this->input->post('alasan'),
			'e_peg_hukuman_ket' => $this->input->post('keterangan')
		);
		return $this->db->insert('tm_peg_hukuman', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg_hukuman', $this->input->post('id'));
		$query=$this->db->delete('tm_peg_hukuman');

		return $query;
	}
}