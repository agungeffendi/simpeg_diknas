<?php
/**
 * 
 */
class Pegawai_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_pegawai($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tm_pegawai');
			return $query->result_array();
		}
		$query = $this->db->get_where('tm_pegawai', array('i_peg' => $id));
		return $query->row_array();
	}

	public function insert($img)
	{
		$data = array(
			'i_peg_nip' => $this->input->post('nip'),
			'n_peg_nama' => $this->input->post('fname'),
			'n_peg_gelardpn' => $this->input->post('gdp'),
			'n_peg_gelarblk' => $this->input->post('gblk'),
			'd_peg_lahir' => date("Y-m-d",strtotime($this->input->post('dlahir'))),
			'a_peg_lahir' => $this->input->post('tlahir'),
			'i_sex' => $this->input->post('jk'),
			'i_agama' => $this->input->post('agama'),
			'a_peg_alamat' => $this->input->post('alamatl'),
			'i_peg_telp' => $this->input->post('no_telp'),
			'i_peg_hp' => $this->input->post('hp'),
			'a_peg_email' => $this->input->post('mail'),
			'i_peg_npwp' => $this->input->post('npwp'),
			'i_peg_taspen' => $this->input->post('taspen'),
			'i_peg_askes' => $this->input->post('askes'),
			'i_bank' => $this->input->post('bank'),
			'i_peg_norek' => $this->input->post('norek'),
			'd_peg_tmtcpns' => date("Y-m-d",strtotime($this->input->post('tmtcpns'))),
			'c_peg_skcpns' => $this->input->post('skcpns'),
			'd_peg_tmtcur' => date("Y-m-d",strtotime($this->input->post('tmt'))),
			'c_peg_skpns' => $this->input->post('skpns'),
			'i_marital' => $this->input->post('pernikahan'),
			'c_peg_status' => $this->input->post('status'),
			'c_peg_blood' => $this->input->post('blood'),
			'c_peg_karpeg' => $this->input->post('karpeg'),
			'c_peg_ktp' => $this->input->post('ktp'),
			'i_kelasjab' => $this->input->post('kelasjabatan'),
			'i_jabatan' => $this->input->post('jabatan'),
			'i_unit4' => $this->input->post('u4'),
			'i_unit3' => $this->input->post('u3'),
			'i_unit2' => $this->input->post('u2'),
			'i_pangkat1' => $this->input->post('pangkatcpns'),
			'i_pangkat2' => $this->input->post('pangkat'),
			'i_pendidikan' => $this->input->post('pendidikan'),
			'i_jurusan' => $this->input->post('jurusan'),
			'c_peg_lulus' => $this->input->post('thn_lulus'),
			'i_eselon_unut' => $this->input->post('pangkateselon'),
			'n_peg_foto' => $img
		);
		return $this->db->insert('tm_pegawai', $data);
	}

	public function update()
	{
		if (empty($this->input->post('u2')))
		{
			$data = array(
				'i_peg_nip' => $this->input->post('nip'),
				'n_peg_nama' => $this->input->post('fname'),
				'n_peg_gelardpn' => $this->input->post('gdp'),
				'n_peg_gelarblk' => $this->input->post('gblk'),
				'd_peg_lahir' => date("Y-m-d",strtotime($this->input->post('dlahir'))),
				'a_peg_lahir' => $this->input->post('tlahir'),
				'i_sex' => $this->input->post('jk'),
				'i_agama' => $this->input->post('agama'),
				'a_peg_alamat' => $this->input->post('alamatl'),
				'i_peg_telp' => $this->input->post('no_telp'),
				'i_peg_hp' => $this->input->post('hp'),
				'a_peg_email' => $this->input->post('mail'),
				'i_peg_npwp' => $this->input->post('npwp'),
				'i_peg_taspen' => $this->input->post('taspen'),
				'i_peg_askes' => $this->input->post('askes'),
				'i_bank' => $this->input->post('bank'),
				'i_peg_norek' => $this->input->post('norek'),
				'd_peg_tmtcpns' => date("Y-m-d",strtotime($this->input->post('tmtcpns'))),
				'c_peg_skcpns' => $this->input->post('skcpns'),
				'd_peg_tmtcur' => date("Y-m-d",strtotime($this->input->post('tmt'))),
				'c_peg_skpns' => $this->input->post('skpns'),
				'i_marital' => $this->input->post('pernikahan'),
				'c_peg_status' => $this->input->post('status'),
				'c_peg_blood' => $this->input->post('blood'),
				'c_peg_karpeg' => $this->input->post('karpeg'),
				'c_peg_ktp' => $this->input->post('ktp'),
				'i_kelasjab' => $this->input->post('kelasjabatan'),
				'i_jabatan' => $this->input->post('jabatan'),
				'i_pangkat1' => $this->input->post('pangkatcpns'),
				'i_pangkat2' => $this->input->post('pangkat'),
				'i_pendidikan' => $this->input->post('pendidikan'),
				'i_jurusan' => $this->input->post('jurusan'),
				'c_peg_lulus' => $this->input->post('thn_lulus'),
				'i_eselon_unut' => $this->input->post('pangkateselon'),
			);
		}
		else
		{
			$data = array(
				'i_peg_nip' => $this->input->post('nip'),
				'n_peg_nama' => $this->input->post('fname'),
				'n_peg_gelardpn' => $this->input->post('gdp'),
				'n_peg_gelarblk' => $this->input->post('gblk'),
				'd_peg_lahir' => date("Y-m-d",strtotime($this->input->post('dlahir'))),
				'a_peg_lahir' => $this->input->post('tlahir'),
				'i_sex' => $this->input->post('jk'),
				'i_agama' => $this->input->post('agama'),
				'a_peg_alamat' => $this->input->post('alamatl'),
				'i_peg_telp' => $this->input->post('no_telp'),
				'i_peg_hp' => $this->input->post('hp'),
				'a_peg_email' => $this->input->post('mail'),
				'i_peg_npwp' => $this->input->post('npwp'),
				'i_peg_taspen' => $this->input->post('taspen'),
				'i_peg_askes' => $this->input->post('askes'),
				'i_bank' => $this->input->post('bank'),
				'i_peg_norek' => $this->input->post('norek'),
				'd_peg_tmtcpns' => date("Y-m-d",strtotime($this->input->post('tmtcpns'))),
				'c_peg_skcpns' => $this->input->post('skcpns'),
				'd_peg_tmtcur' => date("Y-m-d",strtotime($this->input->post('tmt'))),
				'c_peg_skpns' => $this->input->post('skpns'),
				'i_marital' => $this->input->post('pernikahan'),
				'c_peg_status' => $this->input->post('status'),
				'c_peg_blood' => $this->input->post('blood'),
				'c_peg_karpeg' => $this->input->post('karpeg'),
				'c_peg_ktp' => $this->input->post('ktp'),
				'i_kelasjab' => $this->input->post('kelasjabatan'),
				'i_jabatan' => $this->input->post('jabatan'),
				'i_unit4' => $this->input->post('u4'),
				'i_unit3' => $this->input->post('u3'),
				'i_unit2' => $this->input->post('u2'),
				'i_pangkat1' => $this->input->post('pangkatcpns'),
				'i_pangkat2' => $this->input->post('pangkat'),
				'i_pendidikan' => $this->input->post('pendidikan'),
				'i_jurusan' => $this->input->post('jurusan'),
				'c_peg_lulus' => $this->input->post('thn_lulus'),
				'i_eselon_unut' => $this->input->post('pangkateselon'),
			);
		}
		$this->db->where('i_peg',$this->input->post('id'));
		$this->db->update('tm_pegawai', $data);

		$this->db->last_query(); die();
	}

	public function update_foto($img)
	{
		$data = array(
			'n_peg_foto' => $img
		);
		$this->db->where('i_peg',$this->input->post('id'));
		$this->db->update('tm_pegawai', $data);
	}

	public function delete()
	{
		$this->db->where('i_peg', $this->input->post('id'));
		$this->db->delete('tm_pegawai');
	}

	public function profil()
	{
		//return $this->db->get_where($table,$where);

		$this->db->select('a.*, b.n_sex, c.n_agama, d.n_marital, e.n_statuspeg, f.n_pdk, g.n_bank, h.n_jabatan')
		->from('tm_pegawai a')
		->join('tr_sex b','a.i_sex=b.i_sex')
		->join('tr_agama c','a.i_agama=c.i_agama')
		->join('tr_marital d','a.i_marital=d.i_marital')
		->join('tr_statuspeg e','a.c_peg_status=e.i_statuspeg')
		->join('tr_pendidikan f','a.i_pendidikan=f.i_pdk')
		->join('tr_bank g','a.i_bank=g.i_bank')
		->join('tr_jabatan h','a.i_jabatan=h.i_jabatan')
		->where('a.i_peg', $this->input->post('nip'));

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}
	public function profil_edit($nip)
	{
		//return $this->db->get_where($table,$where);

		$this->db->select('a.*, b.n_sex, c.n_agama, d.n_marital, e.n_statuspeg, f.n_pdk, g.n_bank, h.n_jabatan')
		->from('tm_pegawai a')
		->join('tr_sex b','a.i_sex=b.i_sex')
		->join('tr_agama c','a.i_agama=c.i_agama')
		->join('tr_marital d','a.i_marital=d.i_marital')
		->join('tr_statuspeg e','a.c_peg_status=e.i_statuspeg')
		->join('tr_pendidikan f','a.i_pendidikan=f.i_pdk')
		->join('tr_bank g','a.i_bank=g.i_bank')
		->join('tr_jabatan h','a.i_jabatan=h.i_jabatan')
		->where('a.i_peg', $nip);

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->row_array();
	}

	public function total_peg()
	{
		$query = $this->db->get('tm_pegawai');
		return $query->num_rows();
	}
	public function jk()
	{
		$query = $this->db->select('COUNT(i_sex) AS total, i_sex')
		->where('i_sex<>', '')
		->group_by('i_sex')
		->get('tm_pegawai');

		return $query->result_array();
	}
	
	public function goldar()
	{
		$query = $this->db->select('COUNT(c_peg_blood) AS total, c_peg_blood')
		->where('c_peg_blood<>', '')
		->group_by('c_peg_blood')
		->get('tm_pegawai');

		#echo $this->db->last_query(); die();
		
		return $query->result_array();
	}

	public function pendidikan()
	{
		$this->db->select('COUNT(a.i_pendidikan) AS total, a.i_pendidikan, b.n_pdk')
		->from('tm_pegawai a')
		->join('tr_pendidikan b','a.i_pendidikan=b.i_pdk')
		->where('a.i_pendidikan <>', '0')
		->group_by('a.i_pendidikan');

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();
		
		return $query->result_array();
	}
	public function marital()
	{
		$this->db->select('COUNT(a.i_marital) AS total, a.i_marital, b.n_marital')
		->from('tm_pegawai a')
		->join('tr_marital b','a.i_marital=b.i_marital')
		//->where('a.i_marital <>', '0')
		->group_by('a.i_marital');

		$query =  $this->db->get();

		//echo $this->db->last_query(); die();

		return $query->result_array();
	}
}