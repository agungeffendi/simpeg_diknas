<?php
/**
 * 
 */
class Jabfung_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_jabfung($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_jabfung');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_jabfung', array('i_jabfung' => $id));
		return $query->row_array();
	}
}