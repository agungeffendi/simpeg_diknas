<?php
/**
 * 
 */
class Diktek_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_diktek($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_diklattek');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_diklattek', array('i_diklattek' => $id));
		return $query->row_array();
	}
}