<?php
/**
 * 
 */
class KodeBank_m extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	public function get_bank($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('tr_bank');
			return $query->result_array();
		}
		$query = $this->db->get_where('tr_bank', array('i_bank' => $id));
		return $query->row_array();
	}
}