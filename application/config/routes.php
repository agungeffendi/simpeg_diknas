<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['dashboard'] = 'dashboard/index';
$route['auth'] = 'auth/index';
$route['pegawai'] = 'pegawai/index';
$route['bank'] = 'bank/index';
$route['dikstruk'] = 'dikstruk/index';
$route['diktek'] = 'diktek/index';
$route['hukuman'] = 'hukuman/index';
$route['jabatan'] = 'jabatan/index';
$route['jabfung'] = 'jabfung/index';
$route['jurusan'] = 'jurusan/index';
$route['pendidikan'] = 'pendidikan/index';
$route['award'] = 'award/index';
$route['unit'] = 'unit/index';
$route['user'] = 'user/index';
$route['tm_keluarga'] = 'tm_keluarga/index';
$route['default_controller'] = 'auth/';
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
