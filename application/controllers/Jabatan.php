<?php
	class Jabatan extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/jabatan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('jabatan/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/jabatan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Diklat Struktural';

			$this->load->view('layout/header',$data);
			$this->load->view('jabatan/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}