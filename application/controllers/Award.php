<?php
	class Award extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/award/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['award'] = $this->award_m->get_award();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('award/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/award/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Penghargaan';

			$this->load->view('layout/header',$data);
			$this->load->view('award/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}