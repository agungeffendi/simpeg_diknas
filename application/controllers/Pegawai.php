<?php
	class Pegawai extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/pegawai/'.$page.'.php')){
				show_404();
			}
			$data['url'] = get_class();
			$data['func'] = $this->uri->segment(2);
			
			$data['title'] = ucfirst($page);
			$data['pegawai'] = $this->pegawai_m->get_pegawai();
			$data['bank'] = $this->kodebank_m->get_bank();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pendidikan'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['agama'] = $this->agama_m->get_agama();
			$data['status'] = $this->status_m->get_status();
			$data['eselon4'] = $this->unit_m->get_unit();
			$data['eselon3'] = $this->unit_m->get_unit();
			$data['eselon2'] = $this->unit_m->get_unit();
			//print_r($data['pegawai']);
			if (empty($this->input->post('submit'))) {
				$this->load->view('layout/header',$data);
				$this->load->view('pegawai/'.$page, $data);
				$this->load->view('layout/footer',$data);
			}else{

				$config['upload_path']   = './uploads/profile'; 
		         $config['allowed_types'] = 'gif|jpg|png'; 
		         $this->load->library('upload', $config);
		         $file_info = $this->upload->data();
				$img = $file_info['file_name']; 

		         if (!$this->upload->do_upload('foto')) {
		         	echo $this->upload->display_errors('<p>', '</p>');
		         }else{
		         	$this->pegawai_m->insert($this->upload->data('full_path'),$this->input->post(),$img);
		         	$this->load->view('layout/header',$data);
					$this->load->view('pegawai/'.$page, $data);
					$this->load->view('layout/footer',$data);
		         }
			}
		}

		public function tambah()
		{
			$data['pegawai'] = $this->pegawai_m->get_pegawai();
			$data['bank'] = $this->kodebank_m->get_bank();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pendidikan'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['agama'] = $this->agama_m->get_agama();
			$data['status'] = $this->status_m->get_status();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/create', $data);
			$this->load->view('layout/footer',$data);
		}

		public function edit()
		{
			$nip=$this->input->post('id');
			$data['pegawai'] = $this->pegawai_m->profil_edit($nip);
			$data['bank'] = $this->kodebank_m->get_bank();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pendidikan'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['agama'] = $this->agama_m->get_agama();
			$data['status'] = $this->status_m->get_status();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/_form', $data);
			$this->load->view('layout/footer',$data);
		}

		public function create()
		{
			//echo $img;
			/*echo "<pre>";
			var_dump($this->input->post());
			echo "</pre>"; die();*/
			$config['upload_path']   = './uploads/profile';
			$data['title'] = 'Form Tambah Pegawai';
			$data['bank'] = $this->kodebank_m->get_bank();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pendidikan'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['agama'] = $this->agama_m->get_agama();
			$data['status'] = $this->status_m->get_status();
			$data['eselon4'] = $this->unit_m->get_unit();
			$data['eselon3'] = $this->unit_m->get_unit();
			$data['eselon2'] = $this->unit_m->get_unit();
			if (empty($this->input->post('submit'))) {
				$this->load->view('layout/header',$data);
				$this->load->view('pegawai/'.$page, $data);
				$this->load->view('layout/footer');
			}else{

				$config['allowed_types'] = 'gif|jpg|png';
	         $path = $_FILES['foto']['name'];
	         $newName = "".$this->input->post('nip')."".".".pathinfo($path, PATHINFO_EXTENSION); 
	         $config['file_name'] = $newName;
	         $this->load->library('upload', $config);
	         $file_info = $this->upload->data();
	         $img = $file_info['file_name'];

	         //echo $img; die();

	         if (!$this->upload->do_upload('foto')) {
	         	echo $this->upload->display_errors('<p>', '</p>');
	         	echo "<pre>";
	         var_dump($this->input->post());
	         echo "</pre>"; die();
	         }else{
	         /*echo "<pre>";
	         var_dump($this->input->post());
	         echo "</pre>"; die();*/
	         	$this->pegawai_m->insert($img,$this->input->post());
	         	echo "<script>window.close();</script>";
	         }
			}
		}
		public function update()
		{
			/*echo "<pre>";
			var_dump($this->input->post());
			echo "</pre>"; die();*/

         	$this->pegawai_m->update($this->input->post());
         	echo "<script>alert('Profile updated.'); window.close();</script>";
		}

		public function edit_foto()
		{
			$config['upload_path']   = './uploads/profile';
			$config['allowed_types'] = 'gif|jpg|png';
	         $path = $_FILES['foto']['name'];
	         $newName = "".$this->input->post('nip')."".".".pathinfo($path, PATHINFO_EXTENSION); 
	         $config['file_name'] = $newName;
	         $this->load->library('upload', $config);
	         $file_info = $this->upload->data();
	         $img = $file_info['file_name'];

	         $last_foto="".FCPATH."uploads/profile/".$this->input->post('lfoto')."";
	         //echo $last_foto; die();

	         if (!$this->upload->do_upload('foto')) {
	         	echo $this->upload->display_errors('<p>', '</p>');
	         }else{
	         	unlink($last_foto);
	         	$this->pegawai_m->update_foto($img,$this->input->post());
	         	echo "<script>alert('Foto Updated.'); window.close();</script>";
	         }
		}

		public function delete()
		{
			$this->pegawai_m->delete($this->input->post());
			$last_foto="".FCPATH."uploads/profile/".$this->input->post('fpoto')."";
			unlink($last_foto);
			echo "<script>alert('Berhasil dihapus.'); window.location = '../pegawai/index'</script>";
		}

		public function profile()
		{
			$this->load->model("tm_keluarga_m");
			$this->load->model("tm_pendidikan_m");
			$this->load->model("tm_pekerjaan_m");
			$this->load->model("tm_pangkat_m");
			$this->load->model("tm_jabatan_m");
			$this->load->model("tm_penghargaan_m");
			$this->load->model("tm_hukuman_m");
			$this->load->model("tm_dikstruk_m");
			$this->load->model("tm_diktek_m");
			$this->load->model("tm_kgb_m");
			$this->load->model("statuskel_m");

			$data['eselon'] = $this->unit_m->get_unit();
			$data['pegawai'] = $this->pegawai_m->profil($this->input->post());
			$data['keluarga'] = $this->tm_keluarga_m->get_keluarga($this->input->post());
			$data['pendidikan'] = $this->tm_pendidikan_m->get_pendidikan($this->input->post());
			$data['d_kerja'] = $this->tm_pekerjaan_m->get_pekerjaan($this->input->post());
			$data['d_pangkat'] = $this->tm_pangkat_m->get_pangkat($this->input->post());
			$data['d_jabatan'] = $this->tm_jabatan_m->get_jabatan($this->input->post());
			$data['d_penghargaan'] = $this->tm_penghargaan_m->get_penghargaan($this->input->post());
			$data['d_hukuman'] = $this->tm_hukuman_m->get_hukuman($this->input->post());
			$data['d_diklatstruk'] = $this->tm_dikstruk_m->get_diklat($this->input->post());
			$data['d_diklattek'] = $this->tm_diktek_m->get_diklat($this->input->post());
			$data['d_kgb'] = $this->tm_kgb_m->get_kgb($this->input->post());

			$data['statuskel'] = $this->statuskel_m->get_statuskel();
			$data['jenjang'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['penghargaan'] = $this->award_m->get_award();
			$data['hukuman'] = $this->hukuman_m->get_hukuman();
			$data['dikstruk'] = $this->dikstruk_m->get_dikstruk();
			$data['diktek'] = $this->diktek_m->get_diktek();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/profile_', $data);
			$this->load->view('layout/footer');
		}

		public function call_post()
		{
			$data = $this->input->post();
			//echo $data;
			echo json_encode($data);
		}

		public function eselon()
		{
			$id=$this->input->post('id');
			$data=$this->unit_m->get_unit_id($id);
			echo json_encode($data);
		}
	}