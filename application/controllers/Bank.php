<?php
	class Bank extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/bank/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['bank'] = $this->bank_m->get_bank();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('bank/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/bank/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Bank';

			$this->load->view('layout/header',$data);
			$this->load->view('bank/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}