<?php
	class Jurusan extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/jurusan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('jurusan/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/jurusan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jurusan';

			$this->load->view('layout/header',$data);
			$this->load->view('jurusan/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}