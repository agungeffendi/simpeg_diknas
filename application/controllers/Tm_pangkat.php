<?php
	class Tm_pangkat extends CI_Controller{
		public function get(){
			$postdata=$this->input->post();

			$data=$this->user_m->get_keluarga($postdata);

			echo json_encode($data);
		}

		public function create()
		{
			//var_dump($this->input->post());
			$this->load->model('tm_pangkat_m');
			if ($this->tm_pangkat_m->insert($this->input->post())) {
				echo "Data berhasil ditambahkan";
			}else{
				echo "Data gagal dirambahkan";
			}
		}

		public function edit()
		{
			$this->load->model('tm_pangkat_m');
			$this->load->model('pangkat_m');
			$data['pkt']=$this->tm_pangkat_m->get_edit($this->input->post());
			$data['nip']=$this->input->post('nip');
			$data['pangkat']=$this->pangkat_m->get_pangkat();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/_edit/pangkat', $data);
			$this->load->view('layout/footer',$data);
		}

		public function delete()
		{
			/*var_dump($this->input->post());*/
			$this->load->model('tm_pangkat_m');
			//var_dump($this->input->post());
			if ($this->tm_pangkat_m->delete($this->input->post())) {
				echo "Data berhasil dihapus";
			}else{
				echo "Data gagal dihapus";
			}
		}
	}