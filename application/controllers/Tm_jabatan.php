<?php
	class Tm_jabatan extends CI_Controller{
		public function get(){
			$postdata=$this->input->post();

			$data=$this->user_m->get_keluarga($postdata);

			echo json_encode($data);
		}

		public function create()
		{
			//var_dump($this->input->post());
			$this->load->model('tm_jabatan_m');
			if ($this->tm_jabatan_m->insert($this->input->post())) {
				echo "Data berhasil ditambahkan";
			}else{
				echo "Data gagal dirambahkan";
			}
		}

		public function delete()
		{
			/*var_dump($this->input->post());*/
			$this->load->model('tm_jabatan_m');
			//var_dump($this->input->post());
			if ($this->tm_jabatan_m->delete($this->input->post())) {
				echo "Data berhasil dihapus";
			}else{
				echo "Data gagal dihapus";
			}
		}
	}