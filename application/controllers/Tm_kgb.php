<?php
	class Tm_kgb extends CI_Controller{
		public function fetch()
		{
			$this->load->model('tm_kgb_m');
			$data=$this->tm_kgb_m->fetch($this->input->post());
			echo "<img src=".base_url()."'uploads/scan/kgb/".$data['scan_sk']."'>";
		}

		public function create()
		{
			$config['upload_path']   = './uploads/scan/kgb';
			$config['allowed_types'] = 'gif|jpg|png';
			$path = $_FILES['scan']['name'];
			$date=date("Ymd",strtotime($this->input->post('tgl_sk')));
			//$sk=$this->input->post('sk');
			$newName = "".$this->input->post('nip')."_".$date."".".".pathinfo($path, PATHINFO_EXTENSION); 
			$config['file_name'] = $newName;
			$this->load->library('upload', $config);
			$file_info = $this->upload->data();
			$img = $file_info['file_name'];
			/*var_dump($this->input->post());
			echo $img;*/

			$this->load->model("tm_keluarga_m");
			$this->load->model("tm_pendidikan_m");
			$this->load->model("tm_pekerjaan_m");
			$this->load->model("tm_pangkat_m");
			$this->load->model("tm_jabatan_m");
			$this->load->model("tm_penghargaan_m");
			$this->load->model("tm_hukuman_m");
			$this->load->model("tm_dikstruk_m");
			$this->load->model("tm_diktek_m");
			$this->load->model("tm_kgb_m");
			$this->load->model("statuskel_m");

			$data['eselon'] = $this->unit_m->get_unit();
			$data['pegawai'] = $this->pegawai_m->profil($this->input->post());
			$data['keluarga'] = $this->tm_keluarga_m->get_keluarga($this->input->post());
			$data['pendidikan'] = $this->tm_pendidikan_m->get_pendidikan($this->input->post());
			$data['d_kerja'] = $this->tm_pekerjaan_m->get_pekerjaan($this->input->post());
			$data['d_pangkat'] = $this->tm_pangkat_m->get_pangkat($this->input->post());
			$data['d_jabatan'] = $this->tm_jabatan_m->get_jabatan($this->input->post());
			$data['d_penghargaan'] = $this->tm_penghargaan_m->get_penghargaan($this->input->post());
			$data['d_hukuman'] = $this->tm_hukuman_m->get_hukuman($this->input->post());
			$data['d_diklatstruk'] = $this->tm_dikstruk_m->get_diklat($this->input->post());
			$data['d_diklattek'] = $this->tm_diktek_m->get_diklat($this->input->post());
			$data['d_kgb'] = $this->tm_kgb_m->get_kgb($this->input->post());

			$data['statuskel'] = $this->statuskel_m->get_statuskel();
			$data['jenjang'] = $this->pendidikan_m->get_pendidikan();
			$data['jurusan'] = $this->jurusan_m->get_jurusan();
			$data['jabatan'] = $this->jabatan_m->get_jabatan();
			$data['pangkat'] = $this->pangkat_m->get_pangkat();
			$data['penghargaan'] = $this->award_m->get_award();
			$data['hukuman'] = $this->hukuman_m->get_hukuman();
			$data['dikstruk'] = $this->dikstruk_m->get_dikstruk();
			$data['diktek'] = $this->diktek_m->get_diktek();

			if (!$this->upload->do_upload('scan')) {
	         	echo $this->upload->display_errors('<p>', '</p>');
	         	/*echo "<pre>";
	         var_dump($this->input->post());
	         echo "</pre>"; die();*/
	        }else{
	         /*echo "<pre>";
	         var_dump($this->input->post());
	         echo "</pre>"; die();*/
	         	$this->tm_kgb_m->insert($img,$this->input->post());
	         	echo "<script>alert('Data tersimpan.');</script>";
	        }
	        $this->load->view('layout/header',$data);
			$this->load->view('pegawai/profile_', $data);
			$this->load->view('layout/footer');
		}
	}