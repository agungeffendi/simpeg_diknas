<?php
	class Tm_hukuman extends CI_Controller{
		public function get(){
			$postdata=$this->input->post();

			$data=$this->user_m->get_hukuman($postdata);

			echo json_encode($data);
		}

		public function create()
		{
			$this->load->model('tm_hukuman_m');
			//var_dump($this->input->post());
			if ($this->tm_hukuman_m->insert($this->input->post())) {
				echo "Data ".$this->input->post('nama')." berhasil ditambahkan";
			}else{
				echo "Data gagal dirambahkan";
			}
		}

		public function delete()
		{
			/*var_dump($this->input->post());*/
			$this->load->model('tm_hukuman_m');
			//var_dump($this->input->post());
			if ($this->tm_hukuman_m->delete($this->input->post())) {
				echo "Data berhasil dihapus";
			}else{
				echo "Data gagal dihapus";
			}
		}
	}