<?php
	class Hukuman extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/hukuman/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['hukuman'] = $this->hukuman_m->get_hukuman();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('hukuman/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/hukuman/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Diklat Struktural';

			$this->load->view('layout/header',$data);
			$this->load->view('hukuman/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}