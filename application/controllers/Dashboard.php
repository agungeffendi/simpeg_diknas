<?php
	class Dashboard extends CI_Controller{
		public function index($page = 'index'){
			$this->load->helper('url');
			if(!file_exists(APPPATH.'views/dashboard/'.$page.'.php')){
				show_404();
			}
			$data['total_peg'] = $this->pegawai_m->total_peg();
			$data['jenis_kel'] = $this->pegawai_m->jk();
			$data['pendidikan'] = $this->pegawai_m->pendidikan();
			$data['goldar'] = $this->pegawai_m->goldar();
			$data['marital'] = $this->pegawai_m->marital();
			$data['title'] = ucfirst($page);

			$this->load->view('layout/header',$data);
			$this->load->view('dashboard/'.$page, $data);
			$this->load->view('layout/footer',$data);
		}
	}