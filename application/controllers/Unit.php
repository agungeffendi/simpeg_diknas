<?php
	class Unit extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/unitkerja/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['unit'] = $this->unit_m->get_unit();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('unitkerja/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/unitkerja/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Unit Kerja';

			$this->load->view('layout/header',$data);
			$this->load->view('unitkerja/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}