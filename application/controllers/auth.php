<?php
	class Auth extends CI_Controller{
		public function __construct(){
			parent::__construct();
			//$this->load->helper("security");
		}
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/auth/'.$page.'.php')){
				show_404();
			}
			//print_r($data['pegawai']);
			$this->load->view('auth/'.$page);
		}
		public function auth_login() {

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['nama'])){
				$this->load->view('dashboard/index');
			}else{
				echo "<script>alert('Password dan Username Tidak Boleh Kosong.');</script>";
				$this->load->view('auth/index');
			}
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
				'username' => $username,
				'password_hash' => sha1($password)
				);
			$cek = $this->user_m->auth("user",$where)->row_array();
			if($cek > 0){

				$data_session = array(
					'nama' => $username,
					'mail' => $cek["email"],
					'id' => $cek["id"]
					);

				$this->session->set_userdata($data_session);
				echo "<script>alert('Selamat Datang'); window.location = '../dashboard/'</script>";
				//$this->load->view('dashboard/index');
			}else{
				echo "<script>alert('Maaf password atau username salah.');</script>";
				$this->load->view('auth/index');
			}
			/*$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
			);
			$result = $this->User_m->auth($data);
			if ($result == TRUE) {

				$username = $this->input->post('username');
				$result = $this->login_database->read_user_information($username);
				if ($result != false) {
					$session_data = array(
					'username' => $result[0]->user_name,
					'email' => $result[0]->user_email,
					);
					// Add user data in session
					$this->session->set_userdata('logged_in', $session_data);
					$this->load->view('page');
					}
			} else {
				$data = array(
				'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('auth', $data);
				}*/
			}
		}
		public function logout(){
			$this->session->sess_destroy();
			echo "<script>alert('Terimakasih, Sampai Nanti...');</script>";
			$this->load->view('auth/index');
		}
	}