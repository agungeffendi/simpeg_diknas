<?php
	class Jabfung extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/jabfung/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['jabfung'] = $this->jabfung_m->get_jabfung();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('jabfung/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/jabfung/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Jabatan Fungsi';

			$this->load->view('layout/header',$data);
			$this->load->view('jabfung/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}