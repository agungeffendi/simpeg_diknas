<?php
	class Pendidikan extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/pendidikan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['pendidikan'] = $this->pendidikan_m->get_pendidikan();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('pendidikan/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/pendidikan/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Pendidikan';

			$this->load->view('layout/header',$data);
			$this->load->view('pendidikan/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}