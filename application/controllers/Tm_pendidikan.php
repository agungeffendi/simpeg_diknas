<?php
	class Tm_pendidikan extends CI_Controller{
		public function get(){
			$postdata=$this->input->post();

			$data=$this->user_m->get_keluarga($postdata);

			echo json_encode($data);
		}

		public function create()
		{
			/*var_dump($this->input->post());*/
			$this->load->model('tm_pendidikan_m');
			//var_dump($this->input->post());
			if ($this->tm_pendidikan_m->insert($this->input->post())) {
				echo "Data berhasil ditambahkan";
			}else{
				echo "Data gagal dirambahkan";
			}
		}

		public function edit()
		{
			$this->load->model('tm_pendidikan_m');
			$this->load->model('pendidikan_m');
			$this->load->model('jurusan_m');
			$data['pddk']=$this->tm_pendidikan_m->get_edit($this->input->post());
			$data['nip']=$this->input->post('nip');
			$data['jenjang']=$this->pendidikan_m->get_pendidikan();
			$data['jurusan']=$this->jurusan_m->get_jurusan();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/_edit/pendidikan', $data);
			$this->load->view('layout/footer',$data);
		}

		public function delete()
		{
			/*var_dump($this->input->post());*/
			$this->load->model('tm_pendidikan_m');
			//var_dump($this->input->post());
			if ($this->tm_pendidikan_m->delete($this->input->post())) {
				echo "Data berhasil dihapus";
			}else{
				echo "Data gagal dihapus";
			}
		}
	}