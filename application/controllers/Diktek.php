<?php
	class Diktek extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/diktek/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['diktek'] = $this->diktek_m->get_diktek();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('diktek/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/diktek/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Diklat Struktural';

			$this->load->view('layout/header',$data);
			$this->load->view('diktek/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}