<?php
	class User extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/user/'.$page.'.php')){
				show_404();
			}
			$data['controlnya'] = ucfirst($this->uri->segment(1));
			$data['title'] = ucfirst($page);
			$data['user'] = $this->user_m->get_user();
			$data['unit'] = $this->unit_m->get_unit();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('user/'.$page, $data);
			$this->load->view('layout/footer');
		}
		public function profile(){

			$nip=$this->session->userdata['nama'];
			$data['controlnya'] = ucfirst($this->uri->segment(1));
			$data['user'] = $this->user_m->get_user($nip);
			$data['unit'] = $this->unit_m->get_unit();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('user/profile', $data);
			$this->load->view('layout/footer');
		}

		public function create()
		{
			$data['controlnya'] = ucfirst($this->uri->segment(1));

			$data['user'] = $this->user_m->get_user();
			$data['unit'] = $this->unit_m->get_unit();

			$this->form_validation->set_rules('username', 'Username','trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password','trim|required|xss_clean');

			if ($this->form_validation->run() == false) {
			$this->load->view('layout/header',$data);
			$this->load->view('user/index', $data);
			$this->load->view('layout/footer');
			}else{
				$nip=$this->input->post('username');
				$cek = $this->user_m->get_user($nip);

				if ($cek>0) {
					echo "<script>alert('Username Sudah Ada.'); window.location = '../user/'</script>";
				}else{
					$this->user_m->create_user($this->input->post());
					echo "<script>alert('Berhasil Menambahkan User.'); window.location = '../user/'</script>";
				}
			}
		}

		public function update(){

			$data['controlnya'] = ucfirst($this->uri->segment(1));

			$data['user'] = $this->user_m->get_user();
			$data['unit'] = $this->unit_m->get_unit();

			$this->form_validation->set_rules('password', 'Password','trim|required|xss_clean');

			if ($this->form_validation->run() == false) {
				$this->load->view('layout/header',$data);
				$this->load->view('user/index', $data);
				$this->load->view('layout/footer');
			}else{
				$nip=$this->input->post('username');
				$cek = $this->user_m->get_user($nip);

				if ($cek>0) {
					echo "<script>alert('Username Sudah Ada.'); window.location = '../user/'</script>";
				}else{
					$this->user_m->update_user($this->input->post());
					echo "<script>alert('Berhasil Merubah User.'); window.location = '../user/'</script>";
				}
			}
		}

		public function delete()
		{
			$data['user'] = $this->user_m->get_user();
			$data['unit'] = $this->unit_m->get_unit();

			$this->user_m->delete($this->input->post());
			echo "<script>alert('User Dihapus.'); window.location = '../user/'</script>";
			$this->load->view('layout/header',$data);
			$this->load->view('user/index', $data);
			$this->load->view('layout/footer');
		}

		public function detail()
		{
			$postdata=$this->input->post();

			$data=$this->user_m->ajax_call($postdata);

			echo json_encode($data);
		}

	}