<?php
	class Tm_keluarga extends CI_Controller{
		public function get(){
			$postdata=$this->input->post();

			$data=$this->user_m->get_keluarga($postdata);

			echo json_encode($data);
		}

		public function create()
		{
			$this->load->model('tm_keluarga_m');
			//var_dump($this->input->post());
			if ($this->tm_keluarga_m->insert($this->input->post())) {
				echo "Data berhasil ditambahkan";
			}else{
				echo "Data gagal ditambahkan";
			}
		}

		public function edit()
		{
			$this->load->model('tm_keluarga_m');
			$this->load->model('statuskel_m');
			$data['keluarga']=$this->tm_keluarga_m->get_edit($this->input->post());
			$data['nip']=$this->input->post('nip');
			$data['statuskel']=$this->statuskel_m->get_statuskel();

			$this->load->view('layout/header',$data);
			$this->load->view('pegawai/_edit/keluarga', $data);
			$this->load->view('layout/footer',$data);
		}

		public function update()
		{
			$this->load->model('tm_keluarga_m');
			//var_dump($this->input->post());
			if ($this->tm_keluarga_m->update($this->input->post())) {
				echo "Data berhasil dirubah";
			}else{
				echo "Data gagal dirubah";
			}
		}

		public function delete()
		{
			$this->load->model('tm_keluarga_m');
			
			if ($this->tm_keluarga_m->delete($this->input->post())) {
				echo $this->db->last_query(); die();
				echo "Data berhasil dirubah";
			}else{
				echo "Data gagal dirubah";
			}
		}
	}