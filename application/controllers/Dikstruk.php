<?php
	class Dikstruk extends CI_Controller{
		public function index($page = 'index'){
			if(!file_exists(APPPATH.'views/dikstruk/'.$page.'.php')){
				show_404();
			}
			$data['title'] = ucfirst($page);
			$data['dikstruk'] = $this->dikstruk_m->get_dikstruk();
			//print_r($data['pegawai']);
			$this->load->view('layout/header',$data);
			$this->load->view('dikstruk/'.$page, $data);
			$this->load->view('layout/footer');
		}

		public function create($page = '_form'){
			if(!file_exists(APPPATH.'views/dikstruk/'.$page.'.php')){
				show_404();
			}
			$data['title'] = 'Form Tambah Jenis Diklat Struktural';

			$this->load->view('layout/header',$data);
			$this->load->view('dikstruk/'.$page, $data);
			$this->load->view('layout/footer');
		}

	}