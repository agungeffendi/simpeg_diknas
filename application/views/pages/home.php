<div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account-location text-info icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Pegawai</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">88888</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-account-location mr-1" aria-hidden="true"></i> Pegawai Aktif
          </p>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account-location text-warning icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Pamong Belajar</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">3455</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-account-location mr-1" aria-hidden="true"></i> Pamong Belajar
          </p>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-poll-box text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">Pensiun</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">5693</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-account-location mr-1" aria-hidden="true"></i> Pensiunan / Pegawai Non Aktif
          </p>
        </div>
      </div>
    </div>
  </div>
<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          
        </div>
      </div>
    </div>
</div>