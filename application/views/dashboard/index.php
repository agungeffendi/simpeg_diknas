<!-- <div class="row clearfix progress-box">
  <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5">
      <div class="progress-box text-center">
         <input type="text" class="knob dial1" value="<?php echo $total_peg; ?>" data-width="120" data-height="120" data-thickness="0.05" data-fgColor="#0099ff" readonly>
        <h5 class="text-blue padding-top-10 weight-500">Total Pegawai</h5>
        <span class="font-14">75% Average <i class="fa fa-line-chart"></i></span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5">
      <div class="progress-box text-center">
         <input type="text" class="knob dial2" value="75" data-width="120" data-height="120" data-thickness="0.05" data-fgColor="#41ccba" readonly>
        <h5 class="text-light-green padding-top-10 weight-500">Kenaikan Jabatan</h5>
        <span class="font-14">75% Average <i class="fa fa-line-chart"></i></span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5">
      <div class="progress-box text-center">
         <input type="text" class="knob dial3" value="90" data-width="120" data-height="120" data-thickness="0.05" data-fgColor="#f56767" readonly>
        <h5 class="text-light-orange padding-top-10 weight-500">Pensiunan</h5>
        <span class="font-14">90% Average <i class="fa fa-line-chart"></i></span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5">
      <div class="progress-box text-center">
         <input type="text" class="knob dial4" value="35" data-width="120" data-height="120" data-thickness="0.05" data-fgColor="#a683eb" readonly>
        <h5 class="text-light-purple padding-top-10 weight-500">Pegawai Non Aktif</h5>
        <span class="font-14">35% Average <i class="fa fa-line-chart"></i></span>
      </div>
    </div>
  </div>
</div> -->

<div class="row">
  <div class="col-lg-4 col-md-12 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5 xs-pd-20-10 height-100-p">
      <h4 class="mb-30">Graph</h4>
      <div id="jk" style="min-width: 300px; min-height: 300px; margin: 0 auto"></div>
      <div class="table-responsive">
        <table class="table table-hoverable">
          <thead>
            <tr>
              <th>#</th>
              <th>Golongan Darah</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach ($jenis_kel as $kel) {
            if ($kel["i_sex"]=1) {
               $label="Laki-Laki";
             }else{
              $label="Perempuan";
             } ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $label; ?></td>
                <td><?php echo $kel["total"]; ?></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-12 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5 xs-pd-20-10 height-100-p">
      <h4 class="mb-30">Graph</h4>
      <div id="goldar" style="min-width: 300px; min-height: 300px; margin: 0 auto"></div>
        <div class="table-responsive">
          <table class="table table-hoverable">
            <thead>
              <tr>
                <th>#</th>
                <th>Golongan Darah</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($goldar as $drh) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $drh["c_peg_blood"]; ?></td>
                  <td><?php echo $drh["total"]; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-12 col-sm-12 mb-30">
    <div class="bg-white pd-30 box-shadow border-radius-5 xs-pd-20-10 height-100-p">
      <h4 class="mb-30">Graph</h4>
      <div id="marital" style="min-width: 300px; min-height: 300px; margin: 0 auto"></div>
        <div class="table-responsive">
          <table class="table table-hoverable">
            <thead>
              <tr>
                <th>#</th>
                <th>Status</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($marital as $sts_nkh) { ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $sts_nkh["n_marital"]; ?></td>
                <td><?php echo $sts_nkh["total"]; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
    </div>
  </div>

  <div class="col-lg-12 col-md-12 col-sm-12 mb-30">
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
      <div id="pdk" style="min-width: 300px; min-height: 300px; margin: 0 auto"></div>
        <div class="table-responsive">
          <table class="tbl-data table table-hoverable">
            <thead>
              <tr>
                <td>#</td>
                <td>Jenjang</td>
                <td>Jumlah</td>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($pendidikan as $pddk) { ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $pddk['n_pdk']; ?></td>
                <td><?php echo $pddk['total']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
    </div>
  </div>
<script type="text/javascript">
    $(".dial1").knob();
    $({animatedVal: 0}).animate({animatedVal: 66}, {
      duration: 3000,
      easing: "swing",
      step: function() {
        $(".dial1").val(Math.ceil(this.animatedVal)).trigger("change");
      }
    });
    $(".dial2").knob();
    $({animatedVal: 0}).animate({animatedVal: 75}, {
      duration: 3000,
      easing: "swing",
      step: function() {
        $(".dial2").val(Math.ceil(this.animatedVal)).trigger("change");
      }
    });
    $(".dial3").knob();
    $({animatedVal: 0}).animate({animatedVal: 90}, {
      duration: 3000,
      easing: "swing",
      step: function() {
        $(".dial3").val(Math.ceil(this.animatedVal)).trigger("change");
      }
    });
    $(".dial4").knob();
    $({animatedVal: 0}).animate({animatedVal: 35}, {
      duration: 3000,
      easing: "swing",
      step: function() {
        $(".dial4").val(Math.ceil(this.animatedVal)).trigger("change");
      }
    });
  </script>
<script type="text/javascript">
            Highcharts.chart('jk', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Jenis Kelamin'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Jenis Kelamin']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($jenis_kel as $jk) { 
                if ($jk['i_sex']==1) {
                  $label='Laki-Laki';
                }else {
                  $label='Perempuan';
                }?>
              {
                  name: "<?php echo $label; ?>",
                  data: [<?php echo $jk['total']; ?>]

              },
              <?php } ?>
              ]
          });

            Highcharts.chart('goldar', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Golongan Darah'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Golongan Darah']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($goldar as $darah) { ?>
              {
                  name: "<?php echo $darah['c_peg_blood']; ?>",
                  data: [<?php echo $darah['total']; ?>]

              },
              <?php } ?>
              ]
          });

          Highcharts.chart('pdk', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Pendidikan'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Golongan Darah']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($pendidikan as $pdk) { ?>
              {
                  name: "<?php echo $pdk['n_pdk']; ?>",
                  data: [<?php echo $pdk['total']; ?>]

              },
              <?php } ?>
              ]
          });

          Highcharts.chart('marital', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Status Perkawinan'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Status Perkawinan']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($marital as $mrt) { ?>
              {
                  name: "<?php echo $mrt['n_marital']; ?>",
                  data: [<?php echo $mrt['total']; ?>]

              },
              <?php } ?>
              ]
          });
          </script>