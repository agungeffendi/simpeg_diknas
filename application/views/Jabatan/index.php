<div class="card card-statistics">
    <div class="card-body">
        <h4 class="card-title">Daftar Jabatan</h4>
        <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
        <hr>
        <div id="master_collapse" class="collapse">
          <form action="#" method="post">
              <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Nama Jenis Jabatan</label>
                  <div class="col-sm-10">
                    <input name="jabatan_nama" type="text" class="form-control" placeholder="Nama Jenis Jabatan" style="width: 50%;">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Eselon</label>
                  <div class="col-sm-10">
                    <input name="jabatan_eselon" type="text" class="form-control" placeholder="Eselon" style="width: 50%;">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Kategori</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="jabatan_kategori" style="width: 30%;">
                        <option>--Pilih--</option>
                        <option value="1">Struktural</option>
                        <option value="2">Fungsional</option>
                    </select>
                  </div>
              </div>
              <button class="btn btn-default" type="submit">Tambahkan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
        <table class="tbl-data table table-hover table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Nama Jabatan</th>
                    <th>Eselon</th>
                    <th>Kategori</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach ($jabatan as $jbtn): ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td></td>
                    <td><?php echo $jbtn['n_jabatan']; ?></td>
                    <td><?php echo $jbtn['i_eselon']; ?></td>
                    <td><?php if ($jbtn['c_jabatan']=='1') { $kat="Struktural"; }else{ $kat="Fungsional"; } echo $kat; ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        </div>
    </div>
  </div>