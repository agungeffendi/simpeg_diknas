<div class="card card-statistics">
        <div class="card-body">
          <h4 class="card-title">Daftar Bank</h4>
          <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambahkan</button>
            <hr>
            <div id="master_collapse" class="collapse">
              <form method="post" action="">
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Kode Bank</label>
                  <div class="col-sm-10">
                    <input name="bank_kode" type="text" class="form-control" placeholder="Kode Bank" style="width: 30%;">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Nama Bank</label>
                  <div class="col-sm-10">
                    <input name="bank_nama" type="text" class="form-control" placeholder="Nama Bank" style="width: 30%;">
                  </div>
                </div>
                <button type="submit" name="submit" class="btn btn-inverse-info btn-fw">Simpan</button>
              </form>
            </div>
            <hr>
          <div class="table-responsive">
          <table class="tbl-data table table-hover table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th></th>
                <th>Kode Bank</th>
                <th>Nama Bank</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($bank as $bang): ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td></td>
                <td><?php echo $bang['i_bank']; ?></td>
                <td><?php echo $bang['n_bank']; ?></td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
          </div>
        </div>
      </div>