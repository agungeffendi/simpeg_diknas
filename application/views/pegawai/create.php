<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
  <div class="clearfix">
    <h4 class="text-blue">Form Tambah Pegawai</h4>
  </div>
  <hr>
      <div class="wizard-content">
        <form class="tab-wizard wizard-circle wizard" id="submitmodal">
          <h5>Data Pribadi</h5>
          <section>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label >NIP Pegawai :</label>
                  <input name="nip" type="text" class="form-control" placeholder="NIP Pegawai">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Gelar Depan :</label>
                  <input name="gdp" type="text" class="form-control" placeholder="Gelar Depan">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Nama Lengkap :</label>
                  <input name="fname" type="text" class="form-control" placeholder="Nama Lengkap">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>Gelar Belakang :</label>
                  <input name="gblk" type="text" class="form-control" placeholder="Gelar Belakang">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tempat Lahir :</label>
                  <input name="tlahir" type="Text" class="form-control" placeholder="Tempat Lahir">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal Lahir :</label>
                  <input name="dlahir" type="Text" class="date-picker form-control" placeholder="Tanggal Lahir">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Status Pernikahan :</label>
                  <select name="pernikahan" class="form-control custom-select2">
                      <option value="">Pilih</option>
                      <option value="1">Belum Menikah</option>
                      <option value="2">Menikah</option>
                      <option value="3">Duda</option>
                      <option value="4">Janda</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Agama :</label>
                  <select name="agama" class="custom-select2 form-control">
                    <option value="">Pilih</option>
                    <?php foreach ($agama as $rel) { ?>
                    <option value="<?php echo $rel['i_agama']; ?>"><?php echo $rel["n_agama"]; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jenis Kelamin :</label>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="laki" name="jk" class="custom-control-input" value="1">
                    <label class="custom-control-label" for="laki">Laki-Laki</label>
                  </div>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="perempuan" name="jk" class="custom-control-input" value="2">
                    <label class="custom-control-label" for="perempuan">Perempuan</label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Golongan Darah :</label>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="blood-A" name="blood" class="custom-control-input" value="A">
                    <label class="custom-control-label" for="blood-A">A</label>
                  </div>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="blood-B" name="blood" class="custom-control-input" value="B">
                    <label class="custom-control-label" for="blood-B">B</label>
                  </div>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="blood-AB" name="blood" class="custom-control-input" value="AB">
                    <label class="custom-control-label" for="blood-AB">AB</label>
                  </div>
                  <div class="custom-control custom-radio mb-5">
                    <input type="radio" id="blood-O" name="blood" class="custom-control-input" value="O">
                    <label class="custom-control-label" for="blood-O">O</label>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- Step 2 -->
          <h5>Alamat dan Kontak</h5>
          <section>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. KTP :</label>
                  <input name="ktp" type="text" class="form-control" placeholder="No. KTP">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. Telp :</label>
                  <input name="no_telp" type="text" class="form-control" placeholder="No. Telp Pegawai">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. HP :</label>
                  <input name="hp" type="text" class="form-control" placeholder="No. HP Pegawai">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Alamat e-mail :</label>
                  <input name="mail" type="text" class="form-control" placeholder="NIP Pegawai">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Alamat Lengkap :</label>
                  <textarea name="alamatl" class="form-control"></textarea>
                </div>
              </div>
            </div>
          </section>
          <!-- Step 3 -->
          <h5>Billing Information</h5>
          <section>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>NPWP :</label>
                  <input name="npwp" type="text" class="form-control" placeholder="NPWP Pegawai">
                </div>
                <div class="form-group">
                  <label>Taspen :</label>
                  <input name="taspen" type="text" class="form-control" placeholder="Taspen Pegawai">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Askes :</label>
                  <input name="askes" type="text" class="form-control" placeholder="Askes Pegawai">
                </div>
                <div class="form-group">
                  <label>Bank :</label>
                  <select name="bank" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                    <option value="">Pilih Bank</option>
                      <?php foreach ($bank as $kbank) { ?>
                      <option value="<?php echo $kbank['i_bank']; ?>"><?php echo $kbank["n_bank"]; ?> (<?php echo $kbank["i_bank"]; ?>)</option>
                      <?php } ?>
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. Rekening :</label>
                  <input name="norek" type="text" class="form-control validate[required]" placeholder="No. Rekening Pegawai">
                </div>
              </div>
            </div>
          </section>
          <!-- Step 4 -->
          <h5>Kepegawaian</h5>
          <section>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Status Pegawai :</label>
                  <select name="status" class="custom-select2">
                      <option value="">Pilih</option>
                      <?php foreach ($status as $stat) { ?>
                      <option value="<?php echo $stat['i_statuspeg']; ?>"><?php echo $stat["n_statuspeg"]; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Pangkat CPNS</label>
                    <select name="pangkatcpns" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                        <option value="">Pilih</option>
                        <?php foreach ($pangkat as $vpangkat) { ?>
                        <option value="<?php echo $vpangkat['i_pangkat']; ?>"><?php echo $vpangkat["n_pangkat"]; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>TMT CPNS</label>
                    <input name="tmtcpns" type="text" class="date-picker form-control" placeholder="TMT CPNS Pegawai">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>SK CPNS</label>
                    <input name="skcpns" type="text" class="form-control" placeholder="SK CPNS Pegawai">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>SK PNS :</label>
                    <input name="skpns" type="text" class="form-control" placeholder="SK PNS Pegawai">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Karpeg :</label>
                    <input name="karpeg" type="text" class="form-control" placeholder="Karpeg Pegawai">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kelas Jabatan :</label>
                    <input name="kelasjabatan" type="text" class="form-control" placeholder="Kelas Jabatan">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Jabatan :</label>
                    <select name="jabatan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                        <option value="">Pilih</option>
                        <?php foreach ($jabatan as $vjabatan) { ?>
                        <option value="<?php echo $vjabatan['i_jabatan']; ?>"><?php echo $vjabatan["n_jabatan"]; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>TMT Pangkat Terakhir :</label>
                    <input name="tmt" type="Text" class="date-picker form-control" placeholder="TMT Pangkat Terakhir Pegawai">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Pangkat Terakhir :</label>
                    <select name="pangkat" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                        <option value="">Pilih</option>
                        <?php foreach ($pangkat as $vpangkat) { ?>
                        <option value="<?php echo $vpangkat['i_pangkat']; ?>"><?php echo $vpangkat["n_pangkat"]; ?></option>
                        <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Unit Eselon :</label>
                    <input type="checkbox" id="submit"><label>Submit Eselon</label>
                    <input type="text" id="single-tree" class="form-control unit4" placeholder="Ketik Atau Cari Unit">
                    <input type="hidden" name="u4" id="unit4" value="">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Eselon 3 :</label>
                    <input type="Text" class="date-picker form-control" id="u3-txt" disabled>
                    <input type="hidden" name="u3" id="unit3" value="">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Eselon 2 :</label>
                    <input type="Text" class="date-picker form-control" id="u2-txt" disabled>
                    <input type="hidden" name="u2" id="unit2" value="">
                  </div>
                </div>
              </div>
          </section>

          <h5>Pendidikan</h5>
          <section>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jenjang :</label>
                  <select name="pendidikan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php foreach ($pendidikan as $pdk) { ?>
                      <option value="<?php echo $pdk['i_pdk']; ?>"><?php echo $pdk["n_pdk"]; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Jurusan :</label>
                  <select name="jurusan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php foreach ($jurusan as $jrsn) { ?>
                      <option value="<?php echo $jrsn['i_jurusan']; ?>"><?php echo $jrsn["n_jurusan"]; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tahun Lulus :</label>
                  <select name="thn_lulus" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </section>
        </form>
      </div>
</div>

<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body text-center font-18">
        <h3 class="mb-20">Upload Foto dan Selesai, Selamat!</h3>
        <div class="mb-30 text-center"><img src="<?php echo base_url(); ?>deskapp/vendors/images/success.png"></div>
        <?php //echo form_open_multipart('pegawai/create'); ?>
        <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>pegawai/create">
        <div class="fallback">
          <input type="file" name="foto"/>
        </div>
        <input id="nip" name="nip" type="hidden">
        <input id="gdp" name="gdp" type="hidden">
        <input id="fname" name="fname" type="hidden">
        <input id="gblk" name="gblk" type="hidden">
        <input id="tlahir" name="tlahir" type="hidden">
        <input id="dlahir" name="dlahir" type="hidden">
        <input id="jk" name="jk" type="hidden">
        <input id="agama" name="agama" type="hidden">
        <input id="blood" name="blood" type="hidden">
        <input id="pernikahan" name="pernikahan" type="hidden">
        <input id="ktp" name="ktp" type="hidden">
        <input id="alamatl" name="alamatl" type="hidden">
        <input id="no_telp" name="no_telp" type="hidden">
        <input id="hp" name="hp" type="hidden">
        <input id="mail" name="mail" type="hidden">
        <input id="npwp" name="npwp" type="hidden">
        <input id="taspen" name="taspen" type="hidden">
        <input id="askes" name="askes" type="hidden">
        <input id="bank" name="bank" type="hidden">
        <input id="norek" name="norek" type="hidden">
        <input id="status" name="status" type="hidden">
        <input id="pangkatcpns" name="pangkatcpns" type="hidden">
        <input id="tmtcpns" name="tmtcpns" type="hidden">
        <input id="skcpns" name="skcpns" type="hidden">
        <input id="skpns" name="skpns" type="hidden">
        <input id="karpeg" name="karpeg" type="hidden">
        <input id="kelasjabatan" name="kelasjabatan" type="hidden">
        <input id="jabatan" name="jabatan" type="hidden">
        <input id="un4" name="u4" type="hidden">
        <input id="un3" name="u3" type="hidden">
        <input id="un2" name="u2" type="hidden">
        <input id="pangkat" name="pangkat" type="hidden">
        <input id="tmt" name="tmt" type="hidden">
        <input id="pendidikan" name="pendidikan" type="hidden">
        <input id="jurusan" name="jurusan" type="hidden">
        <input id="thn_lulus" name="thn_lulus" type="hidden">
      </div>
      <div class="modal-footer justify-content-center">
        <input type="submit" class="btn btn-primary" name="submit" value="simpan">
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/comboTreePlugin.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/icontains.js"></script>
<script type="text/javascript">
  var data = [
  <?php
  $qry=$this->db->query("SELECT * FROM tr_units where lvl=0");
  foreach ($qry->result_array() as $dta) {
    $qr_c1=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
    $cek1=$qr_c1->result_array(); ?>
    //data level 1
    {
    id: <?php echo $dta['id']; ?>,
    title: "<?php echo $dta['name']; ?>",
    //data level 2 jika ada
    <?php if ($cek1>0) {?>
      subs: [
      <?php $qry_2=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
      foreach ($qry_2->result_array() as $dta2) {
        $qr_c2=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        $cek2=$qr_c2->result_array(); ?>
      {
        id: <?php echo $dta2['id']; ?>,
        title: "<?php echo $dta2['name']; ?>",
        //data level 3 jika ada
        <?php if ($cek2>0) { ?>
        subs: [
        <?php $qry_3=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        foreach ($qry_3->result_array() as $dta3) { ?>
        {
          id: <?php echo $dta3['id']; ?>,
          title: "<?php echo $dta3['name']; ?>",
        },
        <?php } ?>
        ]
        <?php }else{} ?>
      },
      <?php } ?>
      ]
      <?php } else{} ?>
    },
    <?php } ?>];

    var comboTree2;
jQuery(document).ready(function($) {

    comboTree2 = $('#single-tree').comboTree({
      source : data,
      isMultiple: false
    });
      $('#submit').on('click', function() {
        var id = comboTree2.getSelectedItemsId();
      $.ajax({
       url:'<?php echo base_url(); ?>pegawai/eselon',
       method: 'POST',
       data: {id: id},
       dataType: 'json',
       success: function(response){
         var idnya = response.id;
         var nama = response.name;
         
         $('#unit3').val(idnya);
         $('#unit4').val(id);
         $('#u3-txt').val(nama);

            $.ajax({
                url:'<?php echo base_url(); ?>pegawai/eselon',
               method: 'POST',
               data: {id: idnya},
               dataType: 'json',
                success: function (data) {
                  var idnya4 = data.id;
                 var nama4 = data.name;
                 
                 $('#unit2').val(idnya4);
                 $('#u2-txt').val(nama4);
                }
            });
       }
     });
    });
  });
</script>