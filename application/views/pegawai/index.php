<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <a href="<?php echo base_url(); ?>pegawai/tambah" class="btn btn-primary" target="_blank">Tambah</a>
    <hr>
  <table class="tbl-data table table-hover table-bordered" id="dynamic-table">
    <thead>
        <tr>
            <th>#</th>
            <th></th>
            <th>NIP</th>
            <th>Nama Lengkap</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>No Telp.</th>
            <th>No HP</th>
            <th>e-mail</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($pegawai as $peg): ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td>
                <form method="post" onsubmit="return confirm('Yakin Hapus Data Pegawai?');" action="<?php echo base_url(); ?>pegawai/delete">
                  <input type="hidden" name="id" value="<?php echo $peg['i_peg']; ?>">
                  <input type="hidden" name="fpoto" value="<?php echo $peg['n_peg_foto']; ?>">
                  <button type="submit" class="btn btn-danger"><span class="fa fa-trash"></span></button>
              </form>
            </td>
            <td>
              <?php $attr = array('target'=>'_blank');
              echo form_open('pegawai/profile', $attr); ?>
              <input type="hidden" name="nip" value="<?php echo $peg['i_peg']; ?>">
              <button type="submit" class="btn btn-info"><?php echo $peg['i_peg_nip']; ?></button>
              <?php echo form_close(); ?>
            </td>
            <td><?php echo $peg['n_peg_gelardpn']; ?> <?php echo $peg['n_peg_nama']; ?> <?php echo $peg['n_peg_gelarblk']; ?></td>
            <td><?php echo $peg['a_peg_lahir']; ?></td>
            <td><?php echo $peg['d_peg_lahir']; ?></td>
            <td><?php echo $peg['i_peg_telp']; ?></td>
            <td><?php echo $peg['i_peg_hp']; ?></td>
            <td><?php echo $peg['a_peg_email']; ?></td>
        </tr>
        <?php endforeach ?>
    </tbody>
</table>
</div>