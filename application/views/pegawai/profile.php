<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Foto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>pegawai/edit_foto">
          <input type="file" name="foto" class="form-control" />
          <input type="hidden" name="id" value="<?php echo $pegawai['i_peg']; ?>">
          <input type="hidden" name="lfoto" value="<?php echo $pegawai['n_peg_foto']; ?>">
          <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg_nip']; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
      </div>
    </div>
  </div>
</div>

<!-- Batas foto -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Keluarga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
        <hr>
        <div id="master_collapse" class="collapse">
          <form id="form_kel">
          <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Nama Anggota Keluarga</label>
                <div class="col-sm-8">
                  <input name="nama" type="text" class="form-control" placeholder="Nama Lengkap" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Status Keluarga</label>
                <div class="col-sm-8">
                  <select name="status" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($statuskel as $skkel) { ?>
                    <option value="<?php echo $skkel['i_statuskel']; ?>"><?php echo $skkel["n_statuskel"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
                <div class="col-sm-8">
                  <input name="ttl" type="text" class="form-control date-picker" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Pekerjaan</label>
                <div class="col-sm-8">
                  <input name="pekerjaan" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <input name="keterangan" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <button type="submit" name="submit" id="save_keluarga" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Nama Kelauraga</th>
              <th>Status Keluarga</th>
              <th>Tanggal Lahir</th>
              <th>Pekerjaan</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($keluarga as $fam) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $fam['n_peg_keluarga']; ?></td>
                  <td><?php echo $fam['n_statuskel']; ?></td>
                  <td><?php echo $fam['d_peg_keluarga_lahir']; ?></td>
                  <td><?php echo $fam['n_peg_keluarga_pekerjaan']; ?></td>
                  <td><?php echo $fam['e_peg_keluarga']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Keluarga -->

<div class="modal fade" id="pendidikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Pendidikan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#pddk">Tambah</button>
        <hr>
        <div id="pddk" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_pddk">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jenjang</label>
                <div class="col-sm-8">
                  <select name="jenjang" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                    <option value="">Pilih</option>
                    <?php foreach ($jenjang as $jjg) { ?>
                    <option value="<?php echo $jjg['i_pdk']; ?>"><?php echo $jjg["n_pdk"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jurusan</label>
                <div class="col-sm-8">
                  <select name="jurusan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                    <option value="">Pilih</option>
                    <?php foreach ($jurusan as $jrsn) { ?>
                    <option value="<?php echo $jrsn['i_jurusan']; ?>"><?php echo $jrsn["n_jurusan"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Instansi</label>
                <div class="col-sm-8">
                  <input name="instansi" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun Lulus</label>
                <div class="col-sm-8">
                  <select name="tlulus" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Jenjang</th>
              <th>Jurusan</th>
              <th>Instansi</th>
              <th>Tahun Lulus</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($pendidikan as $pdk) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $pdk['n_pdk']; ?></td>
                  <td><?php echo $pdk['n_jurusan']; ?></td>
                  <td><?php echo $pdk['a_peg_pdkformal_lokasi']; ?></td>
                  <td><?php echo $pdk['c_peg_pdkformal_lulus']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Pendidikan -->

<div class="modal fade" id="mdl-kerja" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-kerja">Tambah</button>
        <hr>
        <div id="col-kerja" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_kerja" autocomplete="off">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun Mulai</label>
                <div class="col-sm-8">
                  <select name="tawal" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun Selesai</label>
                <div class="col-sm-8">
                  <select name="takhir" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Unit</label>
                <div class="col-sm-8">
                  <input type="text" id="single-tree">
                  <input type="hidden" name="unit" id="valunit">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jabatan</label>
                <div class="col-sm-8">
                  <select name="jabatan" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($jabatan as $jbtn) { ?>
                    <option value="<?php echo $jbtn['i_jabatan']; ?>"><?php echo $jbtn["n_jabatan"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK</label>
                <div class="col-sm-8">
                  <input name="no_sk" id="nosk" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Periode</th>
              <th>Jabatan</th>
              <th>SK Jabatan</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_kerja as $kerja) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $kerja['c_peg_kerja_awal']; ?> - <?php echo $kerja['c_peg_kerja_akhir']; ?></td>
                  <td><?php echo $kerja['n_jabatan']; ?></td>
                  <td><?php echo $kerja['c_peg_kerja_sk']; ?></td>
                  <td><?php echo $kerja['e_peg_kerja_freetext']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Pekerjaan -->

<div class="modal fade" id="mdl-pangkat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-pangkat">Tambah</button>
        <hr>
        <div id="col-pangkat" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_pangkat">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK Pangkat</label>
                <div class="col-sm-8">
                  <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal SK</label>
                <div class="col-sm-8">
                  <input name="tsk" type="text" class="date-picker form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">TMT Pangkat</label>
                <div class="col-sm-8">
                  <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Pangkat</label>
                <div class="col-sm-8">
                  <select name="pangkat" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($pangkat as $pggkt) { ?>
                    <option value="<?php echo $pggkt['i_pangkat']; ?>"><?php echo $pggkt["n_pangkat"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Penandatangan</label>
                <div class="col-sm-8">
                  <input name="ttd" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>No SK</th>
              <th>Tanggal SK</th>
              <th>TMT Pangkat</th>
              <th>Pangakat</th>
              <th>Penandatangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_pangkat as $pkt) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $pkt['c_peg_pangkat_sk']; ?></td>
                  <td><?php echo $pkt['d_peg_pangkat_sk']; ?></td>
                  <td><?php echo $pkt['d_peg_pangkat_tmt']; ?></td>
                  <td><?php echo $pkt['n_pangkat']; ?></td>
                  <td><?php echo $pkt['n_peg_pangkat_tdtgn']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Pangkat -->

<div class="modal fade" id="mdl-jabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-jbtn">Tambah</button>
        <hr>
        <div id="col-jbtn" class="collapse">
          <?php echo validation_errors(); ?>
          <!-- <form id="form_jabatan"> -->
            <form method="post" action="<?php echo base_url(); ?>tm_jabatan/create">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">TMT Jabatan</label>
                <div class="col-sm-8">
                  <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK</label>
                <div class="col-sm-8">
                  <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jabatan</label>
                <div class="col-sm-8">
                  <select name="jabatan" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($jabatan as $jjbtn) { ?>
                    <option value="<?php echo $jjbtn['i_jabatan']; ?>"><?php echo $jjbtn["n_jabatan"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Penandatangan</label>
                <div class="col-sm-8">
                  <input name="ttd" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>TMT Jabatan</th>
              <th>No SK Jabatan</th>
              <th>Nama Jabatan</th>
              <th>Penandatangan</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_jabatan as $jabtn) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $jabtn['d_peg_jabat_tmt']; ?></td>
                  <td><?php echo $jabtn['c_peg_jabat_sk']; ?></td>
                  <td><?php echo $jabtn['n_jabatan']; ?></td>
                  <td><?php echo $jabtn['n_peg_jabat_tdtgn']; ?></td>
                  <td><?php echo $jabtn['e_peg_jabat_freetext']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Jabatan -->

<div class="modal fade" id="mdl-kgb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-kgb">Tambah</button>
        <hr>
        <div id="col-kgb" class="collapse">
          <?php echo validation_errors(); ?>
          <form enctype="multipart/form-data" method="post" action="<?php echo base_url() ?>Tm_kgb/create">
            <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK Lama</label>
                <div class="col-sm-8">
                  <input name="sk_now" type="text" class="form-control" style="width: 70%;" placeholder="No SK Lama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal SK Lama</label>
                <div class="col-sm-8">
                  <input name="tgl_sk_now" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal SK Gapok Lama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal Berlaku SK Lama</label>
                <div class="col-sm-8">
                  <input name="rea_sk_now" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal Berlaku Gapok Lama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Penandatangan SK Lama</label>
                <div class="col-sm-8">
                  <input name="ttd_sk_now" type="text" class="form-control" style="width: 70%;" placeholder="Penandatangan SK Lama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jumlah Gapok Lama</label>
                <div class="col-sm-8">
                  <input name="sal_now" type="text" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Sekarang">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK Baru</label>
                <div class="col-sm-8">
                  <input name="sk" type="text" class="form-control" style="width: 70%;" placeholder="No SK Baru">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal SK Baru</label>
                <div class="col-sm-8">
                  <input name="tgl_sk" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal SK Gapok Baru">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal Berlaku SK Baru</label>
                <div class="col-sm-8">
                  <input name="rea_sk" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal Berlaku Gapok Baru">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Penandatangan SK Baru</label>
                <div class="col-sm-8">
                  <input name="ttd_sk" type="text" class="form-control" style="width: 70%;" placeholder="Penandatangan SK Baru">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jumlah Gapok Baru</label>
                <div class="col-sm-8">
                  <input name="sal" type="text" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Baru">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Scan SK</label>
                <div class="col-sm-8">
                  <input name="scan" type="file" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Baru">
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th></th>
              <th>No. SK Lama</th>
              <th>Tanggal SK Lama</th>
              <th>Tanggal Berlaku</th>
              <th>Penandatangan</th>
              <th>Gapok Lama</th>
              <th>No. SK Baru</th>
              <th>Tanggal SK Baru</th>
              <th>Tanggal Berlaku</th>
              <th>Penandatangan</th>
              <th>Gapok Baru</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_kgb as $kgb) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><img src="<?php echo base_url(); echo 'uploads/scan/kgb/'; echo $kgb['scan_sk']; ?>"></td>
                  <td><?php echo $kgb['sk_lama']; ?></td>
                  <td><?php echo $kgb['tgl_sk_lama']; ?></td>
                  <td><?php echo $kgb['tgl_berlaku_lama']; ?></td>
                  <td><?php echo $kgb['ttd_lama']; ?></td>
                  <td><?php echo number_format($kgb['gapok_lama']); ?></td>
                  <td><?php echo $kgb['sk_baru']; ?></td>
                  <td><?php echo $kgb['tgl_sk_baru']; ?></td>
                  <td><?php echo $kgb['tgl_berlaku_baru']; ?></td>
                  <td><?php echo $kgb['ttd_baru']; ?></td>
                  <td><?php echo number_format($kgb['gapok_baru']); ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Kenaikan Gaji Berkala -->

<div class="modal fade" id="mdl-award" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-awrd">Tambah</button>
        <hr>
        <div id="col-awrd" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_award">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Penghargaan</label>
                <div class="col-sm-8">
                  <select name="award" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($penghargaan as $awwrd) { ?>
                    <option value="<?php echo $awwrd['i_penghargaan']; ?>"><?php echo $awwrd["n_penghargaan"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun</label>
                <div class="col-sm-8">
                  <select name="taward" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea class="form-control" name="keterangan"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Penghargaan</th>
              <th>Tahun</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_penghargaan as $award) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $award['n_penghargaan']; ?></td>
                  <td><?php echo $award['c_peg_penghargaan_tahun']; ?></td>
                  <td><?php echo $award['e_peg_penghargaan']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Penghargaan -->

<div class="modal fade" id="mdl-penalty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-penalty">Tambah</button>
        <hr>
        <div id="col-penalty" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_penalty">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jenis Hukuman</label>
                <div class="col-sm-8">
                  <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($hukuman as $pently) { ?>
                    <option value="<?php echo $pently['i_hukuman']; ?>"><?php echo $pently["n_hukuman"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tanggal SK</label>
                <div class="col-sm-8">
                  <input name="tgl_sk" type="text" class="date-picker form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">No SK</label>
                <div class="col-sm-8">
                  <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Lama Hukuman</label>
                <div class="col-sm-8">
                  <input name="lama_sk" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">TMT Hukuman</label>
                <div class="col-sm-8">
                  <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Referensi Peraturan Pemerintah</label>
                <div class="col-sm-8">
                  <input name="ref" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Alasan</label>
                <div class="col-sm-8">
                  <input name="alasan" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Hukuman</th>
              <th>Tanggal SK</th>
              <th>No SK</th>
              <th>Lama Hukuman</th>
              <th>TMT Hukuman</th>
              <th>Referensi Peraturan Pemerintah</th>
              <th>Alasan</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_hukuman as $penalty) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $penalty['n_hukuman']; ?></td>
                  <td><?php echo $penalty['d_peg_hukuman_sk']; ?></td>
                  <td><?php echo $penalty['i_peg_hukuman_sk']; ?></td>
                  <td><?php echo $penalty['v_peg_hukuman_durasi']; ?></td>
                  <td><?php echo $penalty['d_peg_hukuman_tmt']; ?></td>
                  <td><?php echo $penalty['c_peg_hukuman_refpp']; ?></td>
                  <td><?php echo $penalty['e_peg_hukuman_alasan']; ?></td>
                  <td><?php echo $penalty['e_peg_hukuman_ket']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Batas Hukuman -->

<div class="modal fade" id="mdl-dikstruk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Diklat Struktural</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-dikstruk">Tambah</button>
        <hr>
        <div id="col-dikstruk" class="collapse">
          <form id="form_dikstruk">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jenis Diklat</label>
                <div class="col-sm-8">
                  <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($dikstruk as $diklatstrukt) { ?>
                    <option value="<?php echo $diklatstrukt['i_diklatstruk']; ?>"><?php echo $diklatstrukt["n_diklatstruk"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Lokasi Diklat</label>
                <div class="col-sm-8">
                  <input name="lokasi" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Lama Diklat (Hari)</label>
                <div class="col-sm-8">
                  <input name="lamanya" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun</label>
                <div class="col-sm-8">
                  <select name="tahun" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Diklat</th>
              <th>Tempat Diklat</th>
              <th>Lama Diklat</th>
              <th>Tahun Diklat</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_diklatstruk as $dikstruk) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $dikstruk['n_diklatstruk']; ?></td>
                  <td><?php echo $dikstruk['a_peg_diklatstruk_lokasi']; ?></td>
                  <td><?php $hari=$dikstruk['v_peg_diklatstruk_durasijam']/24; echo "".$hari." Hari"; ?></td>
                  <td><?php echo $dikstruk['c_peg_diklatstruk_tahun']; ?></td>
                  <td><?php echo $dikstruk['e_peg_diklatstruk_freetext']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mdl-diktek" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Diklat Teknis</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" data-toggle="collapse" data-target="#col-diktek">Tambah</button>
        <hr>
        <div id="col-diktek" class="collapse">
          <?php echo validation_errors(); ?>
          <form id="form_diktek">
            <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Jenis Diklat</label>
                <div class="col-sm-8">
                  <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                    <option value="">Pilih</option>
                    <?php foreach ($diktek as $diktek) { ?>
                    <option value="<?php echo $diktek['i_diklattek']; ?>"><?php echo $diktek["n_diklattek"]; ?></option>
                    <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Lokasi Diklat</label>
                <div class="col-sm-8">
                  <input name="lokasi" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Lama Diklat (Hari)</label>
                <div class="col-sm-8">
                  <input name="lamanya" type="text" class="form-control" style="width: 50%;">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Tahun</label>
                <div class="col-sm-8">
                  <select name="tahun" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                      <option value="">Pilih</option>
                      <?php
                      $dn=date("Y");
                      $dl=$dn-50;
                      for ($i=$dl; $i <= $dn; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label">Keterangan</label>
                <div class="col-sm-8">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <button type="submit" name="submit" class="btn btn-success">Simpan</button>
            </form>
        </div><hr>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Diklat</th>
              <th>Lama Diklat</th>
              <th>Tahun Diklat</th>
              <th>Detail</th>
              <th>Keterangan</th>
            </thead>
            <tbody>
              <?php $no=1; foreach ($d_diklattek as $diktek) { ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $diktek['n_diklattek']; ?></td>
                  <td><?php echo $diktek['v_peg_diklattek_durasijam']; ?></td>
                  <td><?php echo $diktek['c_peg_diklattek_tahun']; ?></td>
                  <td><?php echo $diktek['e_peg_diklattek']; ?></td>
                  <td><?php echo $diktek['e_diklattek_freetext']; ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

  <!-- Default Light Table -->
  <div class="row">
    <div class="col-lg-4">
      
        <div class="card-header border-bottom text-center">
          <div class="mb-3 mx-auto">
            <?php if (empty($pegawai["n_peg_foto"])) { ?>
              <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/profile/def.png" alt="User Avatar" width="110"> </div>
            <?php } elseif (!empty($pegawai["n_peg_foto"])) { ?>
              <a href="#" data-toggle="modal" data-target="#foto"><img class="rounded-circle" src="<?php echo base_url(); ?>uploads/profile/<?php echo $pegawai['n_peg_foto']; ?>" width="110"></a> </div>
            <?php } ?>
          <h4 class="mb-0"><?php echo $pegawai['n_peg_gelardpn']; ?> <?php echo $pegawai['n_peg_nama']; ?> <?php echo $pegawai['n_peg_gelarblk']; ?></h4>
          <span class="text-muted d-block mb-2"><?php echo $pegawai['a_peg_email']; ?></span>
          <form method="post" action="<?php echo base_url(); ?>pegawai/edit">
            <input type="hidden" name="id" value="<?php echo $pegawai['i_peg']; ?>">
            <button type="submit" class="btn btn-secondary">Edit <span class="fa fa-pencil"></span></button>
          </form>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item p-4">
            <strong class="text-muted d-block mb-2">Rincian</strong>
            <?php //var_dump($keluarga); ?>
            <div class="list-group">
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#exampleModal">Kelauarga</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#pendidikan">Pendidikan</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-kerja">Pekerjaan</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-pangkat">Pangkat</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-jabatan">Jabatan</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-kgb">Kenaikan Gaji Berkala</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-award">Penghargaan</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-penalty">Hukuman</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-dikstruk">Diklat Struktural</a>
              <a href="#" class="list-group-item list-group-item-action" data-toggle="modal" data-target="#mdl-diktek">Diklat Teknis</a>
            </div>
          </li>
        </ul>
      
    </div>
    <div class="col-lg-8">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0">Detail Pegawai</h6>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item p-3">
            <div class="row">
              <div class="col">
                <table class="table table-hover">
                  <tr>
                    <td>NIP</td>
                    <td>: <?php echo $pegawai['i_peg_nip']; ?></td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    <td>: <?php echo $pegawai['n_peg_gelardpn']; ?> <?php echo $pegawai['n_peg_nama']; ?> <?php echo $pegawai['n_peg_gelarblk']; ?></td>
                  </tr>
                  <tr>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>: <?php echo $pegawai['a_peg_lahir']; ?>, <?php echo $pegawai['d_peg_lahir']; ?></td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>: <?php echo $pegawai['n_sex']; ?></td>
                  </tr>
                  <tr>
                    <td>Agama</td>
                    <td>: <?php echo $pegawai['n_agama']; ?></td>
                  </tr>
                  <tr>
                    <td>Golongan Darah</td>
                    <td>: <?php echo $pegawai['c_peg_blood']; ?></td>
                  </tr>
                  <tr>
                    <td>Status Penikahan</td>
                    <td>: <?php echo $pegawai['n_marital']; ?></td>
                  </tr>
                  <tr>
                    <td>No. KTP</td>
                    <td>: <?php echo $pegawai['c_peg_ktp']; ?></td>
                  </tr>
                  <tr>
                    <td>Alamat Lengkap</td>
                    <td>: <?php echo $pegawai['a_peg_alamat']; ?></td>
                  </tr>
                  <tr>
                    <td>No Telp.</td>
                    <td>: <?php echo $pegawai['i_peg_telp']; ?></td>
                  </tr>
                  <tr>
                    <td>No HP</td>
                    <td>: <?php echo $pegawai['i_peg_hp']; ?></td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>: <?php echo $pegawai['a_peg_email']; ?></td>
                  </tr>
                  <tr>
                    <td>No NPWP</td>
                    <td>: <?php echo $pegawai['i_peg_npwp'] ?></td>
                  </tr>
                  <tr>
                    <td>No Taspen</td>
                    <td>: <?php echo $pegawai['i_peg_taspen']; ?></td>
                  </tr>
                  <tr>
                    <td>No Askes</td>
                    <td>: <?php echo $pegawai['i_peg_askes']; ?></td>
                  </tr>
                  <tr>
                    <td>Bank</td>
                    <td>: <?php echo $pegawai['n_bank']; ?> (<?php echo $pegawai['i_bank']; ?>)</td>
                  </tr>
                  <tr>
                    <td>No. Rekening</td>
                    <td>: <?php echo $pegawai['i_peg_norek']; ?></td>
                  </tr>
                  <tr>
                    <td>TMT CPNS</td>
                    <td>: <?php echo $pegawai['d_peg_tmtcpns'] ?></td>
                  </tr>
                  <tr>
                    <td>SK CPNS</td>
                    <td>: <?php echo $pegawai['c_peg_skcpns']; ?></td>
                  </tr>
                  <tr>
                    <td>TMT Pegawai</td>
                    <td>: <?php echo $pegawai['d_peg_tmtcur']; ?></td>
                  </tr>
                  <tr>
                    <td>SK PNS</td>
                    <td>: <?php echo $pegawai['c_peg_skpns']; ?></td>
                  </tr>
                  <tr>
                    <td>Jabatan</td>
                    <td>: <?php echo $pegawai['n_jabatan']; ?></td>
                  </tr>
                  <tr>
                    <td>Pangkat CPNS</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Pangkat Terakhir</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Eselon Unit</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Pendidikan Terakhir</td>
                    <td><?php echo $pegawai['n_pdk']; ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $("#form_kel").submit(function(e){
      e.preventDefault();
      var data = $("#form_kel").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_keluarga/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_kel').reset();
                  $('exampleModal').modal('hide');
             }
      });
    });

    $("#form_pddk").submit(function(e){
      e.preventDefault();
      var data = $("#form_pddk").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pendidikan/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_kel').reset();
                  $('pendidikan').modal('hide');
             }
      });
    });

    $("#form_kerja").submit(function(e){
      e.preventDefault();
      var data = $("#form_kerja").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pekerjaan/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_kerja').reset();
                  $('mdl-kerja').modal('hide');
             }
      });
    });

    $("#form_pangkat").submit(function(e){
      e.preventDefault();
      var data = $("#form_pangkat").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pangkat/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_pangkat').reset();
                  $('mdl-pangkat').modal('hide');
             }
      });
    });

    $("#form_jabatan").submit(function(e){
      e.preventDefault();
      var data = $("#form_jabatan").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_jabatan/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_jabatan').reset();
                  $('mdl-jabatan').modal('hide');
             }
      });
    });

    $("#form_award").submit(function(e){
      e.preventDefault();
      var data = $("#form_award").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_penghargaan/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_award').reset();
                  $('mdl-award').modal('hide');
             }
      });
    });

    $("#form_penalty").submit(function(e){
      e.preventDefault();
      var data = $("#form_penalty").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_hukuman/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_penalty').reset();
                  $('mdl-penalty').modal('hide');
             }
      });
    });

    $("#form_dikstruk").submit(function(e){
      e.preventDefault();
      var data = $("#form_dikstruk").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_dikstruk/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_dikstruk').reset();
                  $('mdl-dikstruk').modal('hide');
             }
      });
    });

    $("#form_diktek").submit(function(e){
      e.preventDefault();
      var data = $("#form_diktek").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_diktek/create",
             success: function(data){
                  alert("Info: " + data);
                  document.getElementById('form_diktek').reset();
                  $('mdl-diktek').modal('hide');
             }
      });
    });
  </script>