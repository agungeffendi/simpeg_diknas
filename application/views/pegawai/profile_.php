<div class="modal fade" id="foto" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">Edit Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>pegawai/edit_foto">
          <input type="file" name="foto" class="form-control" />
          <input type="hidden" name="id" value="<?php echo $pegawai['i_peg']; ?>">
          <input type="hidden" name="lfoto" value="<?php echo $pegawai['n_peg_foto']; ?>">
          <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg_nip']; ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="scan_kgb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Foto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="result"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center font-18">
        <h3 class="mb-20">Sukses!</h3>
        <div class="mb-30 text-center"><img src="<?php echo base_url(); ?>deskapp/vendors/images/success.png"></div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-lg-4">
  
        <div class="pd-20 bg-white border-radius-4 box-shadow">
          <center>
              <?php if (empty($pegawai["n_peg_foto"])) { ?>
                <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/profile/def.png" alt="User Avatar" width="150"> 
              <?php } elseif (!empty($pegawai["n_peg_foto"])) { ?>
                <a href="#" data-toggle="modal" data-target="#foto">
                <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/profile/<?php echo $pegawai['n_peg_foto']; ?>" width="150"></a>
              <?php }else{ ?>
                <img class="img-thumbnail" src="<?php echo base_url(); ?>uploads/profile/def.png" alt="User Avatar" width="150">
              <?php } ?>
          </center>
          <hr>
          <h4 class="text-center"><?php echo $pegawai['n_peg_gelardpn']; ?> <?php echo $pegawai['n_peg_nama']; ?> <?php echo $pegawai['n_peg_gelarblk']; ?></h4>
          <p class="text-muted text-center"><?php echo $pegawai['a_peg_email']; ?></p>
          <center>
            <form method="post" action="<?php echo base_url(); ?>pegawai/edit">
              <input type="hidden" name="id" value="<?php echo $pegawai['i_peg']; ?>">
              <button type="submit" class="btn btn-secondary">Edit <span class="fa fa-pencil"></span></button>
            </form>
          </center>
        </div>
      
    </div>
    <div class="col-lg-8">
      <div class="card card-small mb-4">
        <ul class="nav nav-pills">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#home">Data Pribadi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu1">Keluarga</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu2">Pendidikan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu3">Pekerjaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu4">Pangkat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu5">Jabatan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu6">KGB</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu7">Penghargaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu8">Hukuman</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu9">Diklat Struktural</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#menu10">Diklat Teknis</a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane container active" id="home">
            <hr>
            <table class="table table-hover">
              <tr>
                <td>NIP</td>
                <td>: <?php echo $pegawai['i_peg_nip']; ?></td>
              </tr>
              <tr>
                <td>Nama Lengkap</td>
                <td>: <?php echo $pegawai['n_peg_gelardpn']; ?> <?php echo $pegawai['n_peg_nama']; ?> <?php echo $pegawai['n_peg_gelarblk']; ?></td>
              </tr>
              <tr>
                <td>Tempat, Tanggal Lahir</td>
                <td>: <?php echo $pegawai['a_peg_lahir']; ?>, <?php echo $pegawai['d_peg_lahir']; ?></td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td>: <?php echo $pegawai['n_sex']; ?></td>
              </tr>
              <tr>
                <td>Agama</td>
                <td>: <?php echo $pegawai['n_agama']; ?></td>
              </tr>
              <tr>
                <td>Golongan Darah</td>
                <td>: <?php echo $pegawai['c_peg_blood']; ?></td>
              </tr>
              <tr>
                <td>Status Penikahan</td>
                <td>: <?php echo $pegawai['n_marital']; ?></td>
              </tr>
              <tr>
                <td>No. KTP</td>
                <td>: <?php echo $pegawai['c_peg_ktp']; ?></td>
              </tr>
              <tr>
                <td>Alamat Lengkap</td>
                <td>: <?php echo $pegawai['a_peg_alamat']; ?></td>
              </tr>
              <tr>
                <td>No Telp.</td>
                <td>: <?php echo $pegawai['i_peg_telp']; ?></td>
              </tr>
              <tr>
                <td>No HP</td>
                <td>: <?php echo $pegawai['i_peg_hp']; ?></td>
              </tr>
              <tr>
                <td>Email</td>
                <td>: <?php echo $pegawai['a_peg_email']; ?></td>
              </tr>
              <tr>
                <td>No NPWP</td>
                <td>: <?php echo $pegawai['i_peg_npwp'] ?></td>
              </tr>
              <tr>
                <td>No Taspen</td>
                <td>: <?php echo $pegawai['i_peg_taspen']; ?></td>
              </tr>
              <tr>
                <td>No Askes</td>
                <td>: <?php echo $pegawai['i_peg_askes']; ?></td>
              </tr>
              <tr>
                <td>Bank</td>
                <td>: <?php echo $pegawai['n_bank']; ?> (<?php echo $pegawai['i_bank']; ?>)</td>
              </tr>
              <tr>
                <td>No. Rekening</td>
                <td>: <?php echo $pegawai['i_peg_norek']; ?></td>
              </tr>
              <tr>
                <td>TMT CPNS</td>
                <td>: <?php echo $pegawai['d_peg_tmtcpns'] ?></td>
              </tr>
              <tr>
                <td>SK CPNS</td>
                <td>: <?php echo $pegawai['c_peg_skcpns']; ?></td>
              </tr>
              <tr>
                <td>TMT Pegawai</td>
                <td>: <?php echo $pegawai['d_peg_tmtcur']; ?></td>
              </tr>
              <tr>
                <td>SK PNS</td>
                <td>: <?php echo $pegawai['c_peg_skpns']; ?></td>
              </tr>
              <tr>
                <td>Jabatan</td>
                <td>: <?php echo $pegawai['n_jabatan']; ?></td>
              </tr>
              <!-- <tr>
                <td>Pangkat CPNS</td>
                <td></td>
              </tr>
              <tr>
                <td>Pangkat Terakhir</td>
                <td></td>
              </tr> -->
              <tr>
                <td>Pendidikan Terakhir</td>
                <td>: <?php echo $pegawai['n_pdk']; ?></td>
              </tr>
            </table>
          </div>

          <div class="tab-pane container fade" id="menu1">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
            <hr>
            <div id="master_collapse" class="collapse">
              <form autocomplete="off" id="form_kel">
              <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Nama Anggota Keluarga</label>
                    <div class="col-sm-8">
                      <input name="nama" type="text" class="form-control" placeholder="Nama Lengkap" style="width: 50%;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Status Keluarga</label>
                    <div class="col-sm-8">
                      <select name="status" class="custom-select2 form-control" style="width: 100%;">
                        <option value="">Pilih</option>
                        <?php foreach ($statuskel as $skkel) { ?>
                        <option value="<?php echo $skkel['i_statuskel']; ?>"><?php echo $skkel["n_statuskel"]; ?></option>
                        <?php } ?>
                    </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-8">
                      <input name="ttl" type="text" class="form-control date-picker" style="width: 50%;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Pekerjaan</label>
                    <div class="col-sm-8">
                      <input name="pekerjaan" type="text" class="form-control" style="width: 50%;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Keterangan</label>
                    <div class="col-sm-8">
                      <textarea name="keterangan" class="form-control"></textarea>
                    </div>
                  </div>
                  <button type="submit" name="submit" id="save_keluarga" class="btn btn-success">Simpan</button>
                </form>
            </div><hr>
            <div class="table-responsive">
              <table class="tbl-data table table-hover">
                <thead>
                  <th>#</th>
                  <th></th>
                  <th>Nama Kelauraga</th>
                  <th>Status Keluarga</th>
                  <th>Tanggal Lahir</th>
                  <th>Pekerjaan</th>
                  <th>Keterangan</th>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($keluarga as $fam) { ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td>
                        <div class="row">
                          <button data-id="<?php echo $fam['i_peg_keluarga']; ?>" class="fam-del btn btn-danger"><span class="fa fa-trash"></span></button>
                          <form method="post" action="<?php echo base_url(); ?>tm_keluarga/edit">
                            <input type="hidden" name="id" value="<?php echo $fam['i_peg_keluarga']; ?>">
                            <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                            <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                          </form>
                        </div>
                      </td>
                      <td><?php echo $fam['n_peg_keluarga']; ?></td>
                      <td><?php echo $fam['n_statuskel']; ?></td>
                      <td><?php echo $fam['d_peg_keluarga_lahir']; ?></td>
                      <td><?php echo $fam['n_peg_keluarga_pekerjaan']; ?></td>
                      <td><?php echo $fam['e_peg_keluarga']; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="tab-pane container fade" id="menu2">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#pddk">Tambah</button>
              <hr>
              <div id="pddk" class="collapse">
                <form id="form_pddk" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jenjang</label>
                      <div class="col-sm-8">
                        <select name="jenjang" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                          <option value="">Pilih</option>
                          <?php foreach ($jenjang as $jjg) { ?>
                          <option value="<?php echo $jjg['i_pdk']; ?>"><?php echo $jjg["n_pdk"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jurusan</label>
                      <div class="col-sm-8">
                        <select name="jurusan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                          <option value="">Pilih</option>
                          <?php foreach ($jurusan as $jrsn) { ?>
                          <option value="<?php echo $jrsn['i_jurusan']; ?>"><?php echo $jrsn["n_jurusan"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Instansi</label>
                      <div class="col-sm-8">
                        <input name="instansi" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun Lulus</label>
                      <div class="col-sm-8">
                        <select name="tlulus" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover" id="tb-pdk">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Jenjang</th>
                    <th>Jurusan</th>
                    <th>Instansi</th>
                    <th>Tahun Lulus</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($pendidikan as $pdk) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $pdk['i_peg_pdkformal']; ?>" class="pdk-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_pendidikan/edit">
                              <input type="hidden" name="id" value="<?php echo $pdk['i_peg_pdkformal']; ?>">
                              <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $pdk['n_pdk']; ?></td>
                        <td><?php echo $pdk['n_jurusan']; ?></td>
                        <td><?php echo $pdk['a_peg_pdkformal_lokasi']; ?></td>
                        <td><?php echo $pdk['c_peg_pdkformal_lulus']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu3">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-kerja">Tambah</button>
              <hr>
              <div id="col-kerja" class="collapse">
                <form id="form_kerja" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun Mulai</label>
                      <div class="col-sm-8">
                        <select name="tawal" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun Selesai</label>
                      <div class="col-sm-8">
                        <select name="takhir" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Unit</label>
                      <div class="col-sm-8">
                        <input type="text" id="single-tree" class="form-control unit4" placeholder="Ketik Atau Cari Unit">
                        <input type="hidden" name="unit" id="valunit">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jabatan</label>
                      <div class="col-sm-8">
                        <select id="kerja-jabtn" name="jabatan" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($jabatan as $jbtn) { ?>
                          <option value="<?php echo $jbtn['i_jabatan']; ?>"><?php echo $jbtn["n_jabatan"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK</label>
                      <div class="col-sm-8">
                        <input name="no_sk" id="nosk" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea name="keterangan" class="form-control"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Periode</th>
                    <th>Jabatan</th>
                    <th>SK Jabatan</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_kerja as $kerja) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $kerja['i_peg_kerja']; ?>" class="kerja-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_pekerjaan/edit">
                              <input type="hidden" name="id" value="<?php echo $kerja['i_peg_kerja']; ?>">
                              <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $kerja['c_peg_kerja_awal']; ?> - <?php echo $kerja['c_peg_kerja_akhir']; ?></td>
                        <td><?php echo $kerja['n_jabatan']; ?></td>
                        <td><?php echo $kerja['c_peg_kerja_sk']; ?></td>
                        <td><?php echo $kerja['e_peg_kerja_freetext']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu4">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-pangkat">Tambah</button>
              <hr>
              <div id="col-pangkat" class="collapse">
                <form id="form_pangkat" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK Pangkat</label>
                      <div class="col-sm-8">
                        <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal SK</label>
                      <div class="col-sm-8">
                        <input name="tsk" type="text" class="date-picker form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">TMT Pangkat</label>
                      <div class="col-sm-8">
                        <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Pangkat</label>
                      <div class="col-sm-8">
                        <select name="pangkat" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($pangkat as $pggkt) { ?>
                          <option value="<?php echo $pggkt['i_pangkat']; ?>"><?php echo $pggkt["n_pangkat"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Penandatangan</label>
                      <div class="col-sm-8">
                        <input name="ttd" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>No SK</th>
                    <th>Tanggal SK</th>
                    <th>TMT Pangkat</th>
                    <th>Pangakat</th>
                    <th>Penandatangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_pangkat as $pkt) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $pkt['i_peg_pangkat']; ?>" class="pangkat-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_pangkat/edit">
                              <input type="hidden" name="id" value="<?php echo $pkt['i_peg_pangkat']; ?>">
                              <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $pkt['c_peg_pangkat_sk']; ?></td>
                        <td><?php echo $pkt['d_peg_pangkat_sk']; ?></td>
                        <td><?php echo $pkt['d_peg_pangkat_tmt']; ?></td>
                        <td><?php echo $pkt['n_pangkat']; ?></td>
                        <td><?php echo $pkt['n_peg_pangkat_tdtgn']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu5">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-jbtn">Tambah</button>
              <hr>
              <div id="col-jbtn" class="collapse">
                <!-- <form id="form_jabatan"> -->
                  <form id="form_jabatan" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">TMT Jabatan</label>
                      <div class="col-sm-8">
                        <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK</label>
                      <div class="col-sm-8">
                        <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jabatan</label>
                      <div class="col-sm-8">
                        <select name="jabatan" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($jabatan as $jjbtn) { ?>
                          <option value="<?php echo $jjbtn['i_jabatan']; ?>"><?php echo $jjbtn["n_jabatan"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Penandatangan</label>
                      <div class="col-sm-8">
                        <input name="ttd" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea name="keterangan" class="form-control"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>TMT Jabatan</th>
                    <th>No SK Jabatan</th>
                    <th>Nama Jabatan</th>
                    <th>Penandatangan</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_jabatan as $jabtn) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $jabtn['i_peg_jabat']; ?>" class="jabatan-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_jabatan/edit">
                              <input type="hidden" name="id" value="<?php echo $jabtn['i_peg_jabat']; ?>">
                              <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $jabtn['d_peg_jabat_tmt']; ?></td>
                        <td><?php echo $jabtn['c_peg_jabat_sk']; ?></td>
                        <td><?php echo $jabtn['n_jabatan']; ?></td>
                        <td><?php echo $jabtn['n_peg_jabat_tdtgn']; ?></td>
                        <td><?php echo $jabtn['e_peg_jabat_freetext']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu6">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-kgb">Tambah</button>
              <hr>
              <div id="col-kgb" class="collapse">
                <form enctype="multipart/form-data" method="post" action="<?php echo base_url() ?>Tm_kgb/create">
                  <input type="hidden" name="nip" value="<?php echo $pegawai['i_peg']; ?>">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK Lama</label>
                      <div class="col-sm-8">
                        <input name="sk_now" type="text" class="form-control" style="width: 70%;" placeholder="No SK Lama">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal SK Lama</label>
                      <div class="col-sm-8">
                        <input name="tgl_sk_now" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal SK Gapok Lama">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal Berlaku SK Lama</label>
                      <div class="col-sm-8">
                        <input name="rea_sk_now" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal Berlaku Gapok Lama">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Penandatangan SK Lama</label>
                      <div class="col-sm-8">
                        <input name="ttd_sk_now" type="text" class="form-control" style="width: 70%;" placeholder="Penandatangan SK Lama">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jumlah Gapok Lama</label>
                      <div class="col-sm-8">
                        <input name="sal_now" type="text" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Sekarang">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK Baru</label>
                      <div class="col-sm-8">
                        <input name="sk" type="text" class="form-control" style="width: 70%;" placeholder="No SK Baru">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal SK Baru</label>
                      <div class="col-sm-8">
                        <input name="tgl_sk" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal SK Gapok Baru">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal Berlaku SK Baru</label>
                      <div class="col-sm-8">
                        <input name="rea_sk" type="text" class="date-picker form-control" style="width: 70%;" placeholder="Tanggal Berlaku Gapok Baru">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Penandatangan SK Baru</label>
                      <div class="col-sm-8">
                        <input name="ttd_sk" type="text" class="form-control" style="width: 70%;" placeholder="Penandatangan SK Baru">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jumlah Gapok Baru</label>
                      <div class="col-sm-8">
                        <input name="sal" type="text" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Baru">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Scan SK</label>
                      <div class="col-sm-8">
                        <input name="scan" type="file" class="form-control" style="width: 70%;" placeholder="Jumlah Gapok Baru">
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th></th>
                    <th>No. SK Lama</th>
                    <th>Tanggal SK Lama</th>
                    <th>Tanggal Berlaku</th>
                    <th>Penandatangan</th>
                    <th>Gapok Lama</th>
                    <th>No. SK Baru</th>
                    <th>Tanggal SK Baru</th>
                    <th>Tanggal Berlaku</th>
                    <th>Penandatangan</th>
                    <th>Gapok Baru</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_kgb as $kgb) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <form method="post" onsubmit="return confirm('Hapus Data?');" action="<?php echo base_url(); ?>tm_kgb/delete">
                              <input type="hidden" name="id" value="<?php echo $kgb['id']; ?>">
                              <button type="submit" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                            </form>
                            <form method="post" action="<?php echo base_url(); ?>tm_kgb/edit">
                              <input type="hidden" name="id" value="<?php echo $kgb['id']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><!-- <a id="btn_scan" class="btn btn-primary" data-toggle="modal" data-target="#scan_kgb" data-id="<?php echo $kgb['id']; ?>">Cek Scan</a> -->
                          <img style="max-width: none!important;" width="200px" src="<?php echo base_url(); echo 'uploads/scan/kgb/'; echo $kgb['scan_sk']; ?>"></td>
                        <td><?php echo $kgb['sk_lama']; ?></td>
                        <td><?php echo $kgb['tgl_sk_lama']; ?></td>
                        <td><?php echo $kgb['tgl_berlaku_lama']; ?></td>
                        <td><?php echo $kgb['ttd_lama']; ?></td>
                        <td><?php echo number_format($kgb['gapok_lama']); ?></td>
                        <td><?php echo $kgb['sk_baru']; ?></td>
                        <td><?php echo $kgb['tgl_sk_baru']; ?></td>
                        <td><?php echo $kgb['tgl_berlaku_baru']; ?></td>
                        <td><?php echo $kgb['ttd_baru']; ?></td>
                        <td><?php echo number_format($kgb['gapok_baru']); ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu7">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-awrd">Tambah</button>
              <hr>
              <div id="col-awrd" class="collapse">
                <form id="form_award" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Penghargaan</label>
                      <div class="col-sm-8">
                        <select name="award" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($penghargaan as $awwrd) { ?>
                          <option value="<?php echo $awwrd['i_penghargaan']; ?>"><?php echo $awwrd["n_penghargaan"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun</label>
                      <div class="col-sm-8">
                        <select name="taward" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea class="form-control" name="keterangan"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Penghargaan</th>
                    <th>Tahun</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_penghargaan as $award) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $award['i_peg_penghargaan']; ?>" class="award-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_penghargaan/edit">
                              <input type="hidden" name="id" value="<?php echo $award['i_peg_penghargaan']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $award['n_penghargaan']; ?></td>
                        <td><?php echo $award['c_peg_penghargaan_tahun']; ?></td>
                        <td><?php echo $award['e_peg_penghargaan']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu8">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-penalty">Tambah</button>
              <hr>
              <div id="col-penalty" class="collapse">
                <form id="form_penalty" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jenis Hukuman</label>
                      <div class="col-sm-8">
                        <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($hukuman as $pently) { ?>
                          <option value="<?php echo $pently['i_hukuman']; ?>"><?php echo $pently["n_hukuman"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tanggal SK</label>
                      <div class="col-sm-8">
                        <input name="tgl_sk" type="text" class="date-picker form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">No SK</label>
                      <div class="col-sm-8">
                        <input name="no_sk" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Lama Hukuman</label>
                      <div class="col-sm-8">
                        <input name="lama_sk" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">TMT Hukuman</label>
                      <div class="col-sm-8">
                        <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Referensi Peraturan Pemerintah</label>
                      <div class="col-sm-8">
                        <input name="ref" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Alasan</label>
                      <div class="col-sm-8">
                        <input name="alasan" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea name="keterangan" class="form-control"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Hukuman</th>
                    <th>Tanggal SK</th>
                    <th>No SK</th>
                    <th>Lama Hukuman</th>
                    <th>TMT Hukuman</th>
                    <th>Referensi Peraturan Pemerintah</th>
                    <th>Alasan</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_hukuman as $penalty) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $penalty['i_peg_hukuman']; ?>" class="hukuman-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_hukuman/edit">
                              <input type="hidden" name="id" value="<?php echo $penalty['i_peg_hukuman']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $penalty['n_hukuman']; ?></td>
                        <td><?php echo $penalty['d_peg_hukuman_sk']; ?></td>
                        <td><?php echo $penalty['i_peg_hukuman_sk']; ?></td>
                        <td><?php echo $penalty['v_peg_hukuman_durasi']; ?></td>
                        <td><?php echo $penalty['d_peg_hukuman_tmt']; ?></td>
                        <td><?php echo $penalty['c_peg_hukuman_refpp']; ?></td>
                        <td><?php echo $penalty['e_peg_hukuman_alasan']; ?></td>
                        <td><?php echo $penalty['e_peg_hukuman_ket']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu9">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-dikstruk">Tambah</button>
              <hr>
              <div id="col-dikstruk" class="collapse">
                <form id="form_dikstruk" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jenis Diklat</label>
                      <div class="col-sm-8">
                        <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($dikstruk as $diklatstrukt) { ?>
                          <option value="<?php echo $diklatstrukt['i_diklatstruk']; ?>"><?php echo $diklatstrukt["n_diklatstruk"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Lokasi Diklat</label>
                      <div class="col-sm-8">
                        <input name="lokasi" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Lama Diklat (Hari)</label>
                      <div class="col-sm-8">
                        <input name="lamanya" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun</label>
                      <div class="col-sm-8">
                        <select name="tahun" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea name="keterangan" class="form-control"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Diklat</th>
                    <th>Tempat Diklat</th>
                    <th>Lama Diklat</th>
                    <th>Tahun Diklat</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_diklatstruk as $dikstruk) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $dikstruk['i_peg_diklatstruk']; ?>" class="dikstruk-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_dikstruk/edit">
                              <input type="hidden" name="id" value="<?php echo $dikstruk['i_peg_diklatstruk']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $dikstruk['n_diklatstruk']; ?></td>
                        <td><?php echo $dikstruk['a_peg_diklatstruk_lokasi']; ?></td>
                        <td><?php $hari=$dikstruk['v_peg_diklatstruk_durasijam']/24; echo "".$hari." Hari"; ?></td>
                        <td><?php echo $dikstruk['c_peg_diklatstruk_tahun']; ?></td>
                        <td><?php echo $dikstruk['e_peg_diklatstruk_freetext']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>

          <div class="tab-pane container fade" id="menu10">
            <hr>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#col-diktek">Tambah</button>
              <hr>
              <div id="col-diktek" class="collapse">
                <form id="form_diktek" autocomplete="off">
                  <input type="hidden" name="i_peg" value="<?php echo $pegawai['i_peg']; ?>">
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Jenis Diklat</label>
                      <div class="col-sm-8">
                        <select name="jenis" class="custom-select2 form-control" style="width: 100%;">
                          <option value="">Pilih</option>
                          <?php foreach ($diktek as $diktek) { ?>
                          <option value="<?php echo $diktek['i_diklattek']; ?>"><?php echo $diktek["n_diklattek"]; ?></option>
                          <?php } ?>
                      </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Lokasi Diklat</label>
                      <div class="col-sm-8">
                        <input name="lokasi" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Lama Diklat (Hari)</label>
                      <div class="col-sm-8">
                        <input name="lamanya" type="text" class="form-control" style="width: 50%;">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Tahun</label>
                      <div class="col-sm-8">
                        <select name="tahun" class="custom-select2 form-control" style="width: 100%; height: 38px;">
                            <option value="">Pilih</option>
                            <?php
                            $dn=date("Y");
                            $dl=$dn-50;
                            for ($i=$dl; $i <= $dn; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-4 col-form-label">Keterangan</label>
                      <div class="col-sm-8">
                        <textarea name="keterangan" class="form-control"></textarea>
                      </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-success">Simpan</button>
                  </form>
              </div><hr>
              <div class="table-responsive">
                <table class="tbl-data table table-hover">
                  <thead>
                    <th>#</th>
                    <th></th>
                    <th>Diklat</th>
                    <th>Lama Diklat</th>
                    <th>Tahun Diklat</th>
                    <th>Detail</th>
                    <th>Keterangan</th>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($d_diklattek as $diktek) { ?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <div class="row">
                            <button data-id="<?php echo $diktek['i_peg_diklattek']; ?>" class="diktek-del btn btn-danger"><span class="fa fa-trash"></span></button>
                            <form method="post" action="<?php echo base_url(); ?>tm_diktek/edit">
                              <input type="hidden" name="id" value="<?php echo $diktek['i_peg_diklattek']; ?>">
                              <button type="submit" class="btn btn-warning"><span class="fa fa-gear"></span></button>
                            </form>
                          </div>
                        </td>
                        <td><?php echo $diktek['n_diklattek']; ?></td>
                        <td><?php echo $diktek['v_peg_diklattek_durasijam']; ?></td>
                        <td><?php echo $diktek['c_peg_diklattek_tahun']; ?></td>
                        <td><?php echo $diktek['e_peg_diklattek']; ?></td>
                        <td><?php echo $diktek['e_diklattek_freetext']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $("#success-modal").on('hidden.bs.modal',function(){
      location.reload();
    });
    $("#btn_scan").on('click',function(e){
      var data = $("#this").id();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_kgb/fetch",
             success: function(result){
              $('#result').replaceWith(result);
             }
      });
    });

    $("#form_kel").submit(function(e){
      e.preventDefault();
      var data = $("#form_kel").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_keluarga/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".fam-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_keluarga/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_pddk").submit(function(e){
      e.preventDefault();
      var data = $("#form_pddk").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pendidikan/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".pdk-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_pendidikan/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_kerja").submit(function(e){
      e.preventDefault();
      var data = $("#form_kerja").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pekerjaan/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".kerja-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_pekerjaan/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_pangkat").submit(function(e){
      e.preventDefault();
      var data = $("#form_pangkat").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_pangkat/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".pangkat-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_pangkat/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_jabatan").submit(function(e){
      e.preventDefault();
      var data = $("#form_jabatan").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_jabatan/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".jabatan-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_jabatan/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_award").submit(function(e){
      e.preventDefault();
      var data = $("#form_award").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_penghargaan/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".award-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_penghargaan/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_penalty").submit(function(e){
      e.preventDefault();
      var data = $("#form_penalty").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_hukuman/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".hukuman-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_hukuman/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_dikstruk").submit(function(e){
      e.preventDefault();
      var data = $("#form_dikstruk").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_dikstruk/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".dikstruk-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_dikstruk/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });

    $("#form_diktek").submit(function(e){
      e.preventDefault();
      var data = $("#form_diktek").serialize();
      $.ajax({
             data: data,
             type: "post",
             url: "<?php echo base_url(); ?>tm_diktek/create",
             success: function(data){
                  $('#success-modal').modal('show');
             }
      });
    });

    $(".diktek-del").on('click',function(){
      var data = $(this).data('id');
      if (confirm("Delete Data?")) {
        // return alert(data);
        $.ajax({
               type: "post",
               data: "id="+data,
               url: "<?php echo base_url(); ?>tm_diktek/delete",
               success: function(data){
                  $('#success-modal').modal('show');
               }
        });
      }
      return false;
    });
  </script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/comboTreePlugin.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/icontains.js"></script>
  <script type="text/javascript">
  var data = [
  <?php
  $qry=$this->db->query("SELECT * FROM tr_units where lvl=0");
  foreach ($qry->result_array() as $dta) {
    $qr_c1=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
    $cek1=$qr_c1->result_array(); ?>
    //data level 1
    {
    id: <?php echo $dta['id']; ?>,
    title: "<?php echo $dta['name']; ?>",
    //data level 2 jika ada
    <?php if ($cek1>0) {?>
      subs: [
      <?php $qry_2=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
      foreach ($qry_2->result_array() as $dta2) {
        $qr_c2=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        $cek2=$qr_c2->result_array(); ?>
      {
        id: <?php echo $dta2['id']; ?>,
        title: "<?php echo $dta2['name']; ?>",
        //data level 3 jika ada
        <?php if ($cek2>0) { ?>
        subs: [
        <?php $qry_3=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        foreach ($qry_3->result_array() as $dta3) { ?>
        {
          id: <?php echo $dta3['id']; ?>,
          title: "<?php echo $dta3['name']; ?>",
        },
        <?php } ?>
        ]
        <?php }else{} ?>
      },
      <?php } ?>
      ]
      <?php } else{} ?>
    },
    <?php } ?>];

    var comboTree2;
jQuery(document).ready(function($) {

    comboTree2 = $('#single-tree').comboTree({
      source : data,
      isMultiple: false
    });
      $('#kerja-jabtn').on('change', function() {
        var id = comboTree2.getSelectedItemsId();
        $('#valunit').val(id);
    });
  });
</script>