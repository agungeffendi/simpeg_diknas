<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
  <div class="clearfix">
    <h4 class="text-blue">Form Edit Data Keluarga</h4>
  </div>
    <div align="right">
    <?php echo form_open('pegawai/profile'); ?>
    <input type="hidden" name="nip" value="<?php echo $nip; ?>">
    <button type="submit" class="btn btn-info"><- Kembali</button>
    <?php echo form_close(); ?>
  </div>
  <hr>
  <?php //echo "<pre>".var_dump($keluarga)."</pre>"; ?>

  <form autocomplete="off" id="form_kel">
    <input type="hidden" name="i_peg" value="<?php echo $nip; ?>">
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Nama Anggota Keluarga</label>
          <div class="col-sm-8">
            <input name="nama" type="text" class="form-control" placeholder="Nama Lengkap" value="<?php echo $keluarga['n_peg_keluarga']; ?>" style="width: 50%;">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Status Keluarga</label>
          <div class="col-sm-8">
            <select name="status" class="custom-select2 form-control" style="width: 100%;">
              <option value="">Pilih</option>
              <?php foreach ($statuskel as $skkel) { ?>
              <option <?php if($keluarga['i_statuskel']==$skkel['i_statuskel']){echo "selected";} ?> value="<?php echo $skkel['i_statuskel']; ?>"><?php echo $skkel["n_statuskel"]; ?></option>
              <?php } ?>
          </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
          <div class="col-sm-8">
            <?php $tgl_lhr= strtotime($keluarga['d_peg_keluarga_lahir']); ?>
            <input name="ttl" value="<?php echo date('d M Y',$tgl_lhr); ?>" type="text" class="form-control date-picker" style="width: 50%;">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Pekerjaan</label>
          <div class="col-sm-8">
            <input name="pekerjaan" type="text" class="form-control" value="<?php echo $keluarga['n_peg_keluarga_pekerjaan']; ?>" style="width: 50%;">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Keterangan</label>
          <div class="col-sm-8">
            <textarea name="keterangan" class="form-control"><?php echo $keluarga['e_peg_keluarga']; ?></textarea>
          </div>
        </div>
        <button type="submit" name="submit" id="save_keluarga" class="btn btn-success">Simpan</button>
      </form>
</div>
<script>
history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
window.onbeforeunload = function() {
    return "Dude, are you sure you want to leave? Think of the kittens!";
}
</script>