<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
  <div class="clearfix">
    <h4 class="text-blue">Form Edit Data Pendidikan</h4>
  </div>
    <div align="right">
    <?php echo form_open('pegawai/profile'); ?>
    <input type="hidden" name="nip" value="<?php echo $nip; ?>">
    <button type="submit" class="btn btn-info"><- Kembali</button>
    <?php echo form_close(); ?>
  </div>
  <hr>
  <?php //echo "<pre>".var_dump($keluarga)."</pre>"; ?>

  <form id="form_pangkat" autocomplete="off">
    <input type="hidden" name="i_peg" value="<?php echo $nip; ?>">
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">No SK Pangkat</label>
        <div class="col-sm-8">
          <input name="no_sk" type="text" class="form-control" value="<?php echo $pkt['c_peg_pangkat_sk']; ?>" style="width: 50%;">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tanggal SK</label>
        <div class="col-sm-8">
          <?php $tgl_lhr= strtotime($pkt['d_peg_pangkat_sk']); ?>
          <input name="tsk" type="text" class="date-picker form-control" style="width: 50%;" value="<?php echo date('d M Y',$tgl_lhr); ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">TMT Pangkat</label>
        <div class="col-sm-8">
          <?php $tgl_tmt= strtotime($pkt['d_peg_pangkat_tmt']); ?>
          <input name="tmt" type="text" class="date-picker form-control" style="width: 50%;" value="<?php echo date('d M Y',$tgl_tmt); ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Pangkat</label>
        <div class="col-sm-8">
          <select name="pangkat" class="custom-select2 form-control" style="width: 100%;">
            <option value="">Pilih</option>
            <?php foreach ($pangkat as $pggkt) { ?>
            <option <?php if($pkt['i_pangkat']==$pggkt['i_pangkat']){echo "selected";} ?> value="<?php echo $pggkt['i_pangkat']; ?>"><?php echo $pggkt["n_pangkat"]; ?></option>
            <?php } ?>
        </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Penandatangan</label>
        <div class="col-sm-8">
          <input name="ttd" type="text" class="form-control" style="width: 50%;" value="<?php echo $pkt['n_peg_pangkat_tdtgn']; ?>">
        </div>
      </div>
      <button type="submit" name="submit" class="btn btn-success">Simpan</button>
    </form>

<script>
history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
window.onbeforeunload = function() {
    return "Dude, are you sure you want to leave? Think of the kittens!";
}
</script>