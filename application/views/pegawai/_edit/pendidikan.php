<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
  <div class="clearfix">
    <h4 class="text-blue">Form Edit Data Pendidikan</h4>
  </div>
    <div align="right">
    <?php echo form_open('pegawai/profile'); ?>
    <input type="hidden" name="nip" value="<?php echo $nip; ?>">
    <button type="submit" class="btn btn-info"><- Kembali</button>
    <?php echo form_close(); ?>
  </div>
  <hr>
  <?php //echo "<pre>".var_dump($keluarga)."</pre>"; ?>

  <form id="form_pddk" autocomplete="off">
    <input type="hidden" name="i_peg" value="<?php echo $nip; ?>">
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Jenjang</label>
        <div class="col-sm-8">
          <select name="jenjang" class="custom-select2 form-control" style="width: 100%; height: 38px;">
            <option value="">Pilih</option>
            <?php foreach ($jenjang as $jjg) { ?>
            <option <?php if($pddk['i_pdk']==$jjg['i_pdk']){echo "selected";} ?> value="<?php echo $jjg['i_pdk']; ?>"><?php echo $jjg["n_pdk"]; ?></option>
            <?php } ?>
        </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Jurusan</label>
        <div class="col-sm-8">
          <select name="jurusan" class="custom-select2 form-control" style="width: 100%; height: 38px;">
            <option value="">Pilih</option>
            <?php foreach ($jurusan as $jrsn) { ?>
            <option <?php if($pddk['i_jurusan']==$jrsn['i_jurusan']){echo "selected";} ?> value="<?php echo $jrsn['i_jurusan']; ?>"><?php echo $jrsn["n_jurusan"]; ?></option>
            <?php } ?>
        </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Instansi</label>
        <div class="col-sm-8">
          <input name="instansi" type="text" class="form-control" value="<?php echo $pddk['a_peg_pdkformal_lokasi'] ?>" style="width: 50%;">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tahun Lulus</label>
        <div class="col-sm-8">
          <select name="tlulus" class="custom-select2 form-control" style="width: 100%; height: 38px;">
              <option value="">Pilih</option>
              <?php
              $dn=date("Y");
              $dl=$dn-50;
              for ($i=$dl; $i <= $dn; $i++) { ?>
              <option <?php if($pddk['c_peg_pdkformal_lulus']==$i){echo "selected";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php } ?>
          </select>
        </div>
      </div>
      <button type="submit" name="submit" class="btn btn-success">Simpan</button>
    </form>

<script>
history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
window.onbeforeunload = function() {
    return "Dude, are you sure you want to leave? Think of the kittens!";
}
</script>