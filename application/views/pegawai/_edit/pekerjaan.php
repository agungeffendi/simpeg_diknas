<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
  <div class="clearfix">
    <h4 class="text-blue">Form Edit Data Pendidikan</h4>
  </div>
    <div align="right">
    <?php echo form_open('pegawai/profile'); ?>
    <input type="hidden" name="nip" value="<?php echo $nip; ?>">
    <button type="submit" class="btn btn-info"><- Kembali</button>
    <?php echo form_close(); ?>
  </div>
  <hr>
  <?php //echo "<pre>".var_dump($keluarga)."</pre>"; ?>

  <form id="form_kerja" autocomplete="off">
    <input type="hidden" name="i_peg" value="<?php echo $nip; ?>">
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tahun Mulai</label>
        <div class="col-sm-8">
          <select name="tawal" class="custom-select2 form-control" style="width: 100%; height: 38px;">
              <option value="">Pilih</option>
              <?php
              $dn=date("Y");
              $dl=$dn-50;
              for ($i=$dl; $i <= $dn; $i++) { ?>
              <option <?php if($kerja['c_peg_kerja_awal']==$i){echo "selected";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tahun Selesai</label>
        <div class="col-sm-8">
          <select name="takhir" class="custom-select2 form-control" style="width: 100%; height: 38px;">
              <option value="">Pilih</option>
              <?php
              $dn=date("Y");
              $dl=$dn-50;
              for ($i=$dl; $i <= $dn; $i++) { ?>
              <option <?php if($kerja['c_peg_kerja_akhir']==$i){echo "selected";} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
              <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Unit</label>
        <div class="col-sm-8">
          <input type="text" id="single-tree" class="form-control unit4" placeholder="Ketik Atau Cari Unit">
          <input type="hidden" name="unit" id="valunit">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Jabatan</label>
        <div class="col-sm-8">
          <select id="kerja-jabtn" name="jabatan" class="custom-select2 form-control" style="width: 100%;">
            <option value="">Pilih</option>
            <?php foreach ($jabatan as $jbtn) { ?>
            <option <?php if($kerja['i_jabatan']==$jbtn['i_jabatan']){echo "selected";} ?> value="<?php echo $jbtn['i_jabatan']; ?>"><?php echo $jbtn["n_jabatan"]; ?></option>
            <?php } ?>
        </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">No SK</label>
        <div class="col-sm-8">
          <input name="no_sk" id="nosk" type="text" class="form-control" value="<?php echo $kerja['c_peg_kerja_sk']; ?>" style="width: 50%;">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Keterangan</label>
        <div class="col-sm-8">
          <textarea name="keterangan" class="form-control"><?php echo $kerja['e_peg_kerja_freetext']; ?></textarea>
        </div>
      </div>
      <button type="submit" name="submit" class="btn btn-success">Simpan</button>
    </form>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/comboTreePlugin.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/combo-tree/icontains.js"></script>
  <script type="text/javascript">
  var data = [
  <?php
  $qry=$this->db->query("SELECT * FROM tr_units where lvl=0");
  foreach ($qry->result_array() as $dta) {
    $qr_c1=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
    $cek1=$qr_c1->result_array(); ?>
    //data level 1
    {
    id: <?php echo $dta['id']; ?>,
    title: "<?php echo $dta['name']; ?>",
    //data level 2 jika ada
    <?php if ($cek1>0) {?>
      subs: [
      <?php $qry_2=$this->db->query("SELECT * FROM tr_units WHERE lvl='1' AND root=$dta[id]");
      foreach ($qry_2->result_array() as $dta2) {
        $qr_c2=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        $cek2=$qr_c2->result_array(); ?>
      {
        id: <?php echo $dta2['id']; ?>,
        title: "<?php echo $dta2['name']; ?>",
        //data level 3 jika ada
        <?php if ($cek2>0) { ?>
        subs: [
        <?php $qry_3=$this->db->query("SELECT * FROM tr_units WHERE lvl='2' AND root=$dta2[id]");
        foreach ($qry_3->result_array() as $dta3) { ?>
        {
          id: <?php echo $dta3['id']; ?>,
          title: "<?php echo $dta3['name']; ?>",
        },
        <?php } ?>
        ]
        <?php }else{} ?>
      },
      <?php } ?>
      ]
      <?php } else{} ?>
    },
    <?php } ?>];

    var comboTree2;
jQuery(document).ready(function($) {

    comboTree2 = $('#single-tree').comboTree({
      source : data,
      isMultiple: false
    });
      $('#kerja-jabtn').on('change', function() {
        var id = comboTree2.getSelectedItemsId();
        $('#valunit').val(id);
    });
  });
</script>
<script>
history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
window.onbeforeunload = function() {
    return "Dude, are you sure you want to leave? Think of the kittens!";
}
</script>