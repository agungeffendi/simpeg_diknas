<script type="text/javascript">
            Highcharts.chart('jk', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Jenis Kelamin'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Jenis Kelamin']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($jenis_kel as $jk) { 
                if ($jk['i_sex']==1) {
                  $label='Laki-Laki';
                }else {
                  $label='Perempuan';
                }?>
              {
                  name: "<?php echo $label; ?>",
                  data: [<?php echo $jk['total']; ?>]

              },
              <?php } ?>
              ]
          });

            Highcharts.chart('goldar', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Golongan Darah'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Golongan Darah']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($goldar as $darah) { ?>
              {
                  name: "<?php echo $darah['c_peg_blood']; ?>",
                  data: [<?php echo $darah['total']; ?>]

              },
              <?php } ?>
              ]
          });

          Highcharts.chart('pdk', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Golongan Darah'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Golongan Darah']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($pendidikan as $pdk) { ?>
              {
                  name: "<?php echo $pdk['n_pdk']; ?>",
                  data: [<?php echo $pdk['total']; ?>]

              },
              <?php } ?>
              ]
          });

          Highcharts.chart('marital', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Jumlah Pegawai Berdasarkan Status Perkawinan'
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Orang'
                  }
              },
              xAxis: {
                  categories: ['Status Perkawinan']
              },
              credits: {
                  enabled: false
              },
              series: [
              <?php foreach ($marital as $mrt) { ?>
              {
                  name: "<?php echo $mrt['n_marital']; ?>",
                  data: [<?php echo $mrt['total']; ?>]

              },
              <?php } ?>
              ]
          });
          </script>