        </div>
        <div class="footer-wrap bg-white pd-20 mb-20 border-radius-5 box-shadow">
          Copyright &copy; DIKMAS PAUD <?php echo date('Y'); ?>
        </div>
    </div>
  </div>
    <!-- js -->
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/datatables/media/js/dataTables.responsive.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/datatables/media/js/responsive.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/jquery-steps/build/jquery.steps.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/dropzone/src/dropzone.js"></script>
  <script>
    Dropzone.autoDiscover = false;
    $(".dropzone").dropzone({
      addRemoveLinks: true,
      removedfile: function(file) {
        var name = file.name;
        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      }
    });
  </script>

  <script type="text/javascript">
    $('document').ready(function(){
      $('.tbl-data').DataTable({
        scrollCollapse: true,
        autoWidth: false,
        responsive: true,
        columnDefs: [{
          targets: "datatable-nosort",
          orderable: false,
        }],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
          "info": "_START_-_END_ of _TOTAL_ entries",
          searchPlaceholder: "Pencarian"
        },
      });
    });
  </script>
  <script>
    $(".tab-wizard").steps({
      headerTag: "h5",
      bodyTag: "section",
      transitionEffect: "fade",
      titleTemplate: '<span class="step">#index#</span> #title#',
      labels: {
        finish: "Submit"
      },
      onFinished: function (event, currentIndex) {
        $.ajax({
                type : 'POST',
                data: $("#submitmodal").serialize(),
                url : '<?php echo base_url(); ?>pegawai/call_post',
                success : function(data){
                  var obj = JSON.parse(data);
                  console.log(obj);

                  $('#id_peg').val(obj.id);
                  $('#nip').val(obj.nip);
                  $('#gdp').val(obj.gdp);
                  $('#fname').val(obj.fname);
                  $('#gblk').val(obj.gblk);
                  $('#tlahir').val(obj.tlahir);
                  $('#dlahir').val(obj.dlahir);
                  $('#jk').val(obj.jk);
                  $('#agama').val(obj.agama);
                  $('#blood').val(obj.blood);
                  $('#pernikahan').val(obj.pernikahan);
                  $('#ktp').val(obj.ktp);
                  $('#alamatl').val(obj.alamatl);
                  $('#no_telp').val(obj.no_telp);
                  $('#hp').val(obj.hp);
                  $('#mail').val(obj.mail);
                  $('#npwp').val(obj.npwp);
                  $('#taspen').val(obj.taspen);
                  $('#askes').val(obj.askes);
                  $('#bank').val(obj.bank);
                  $('#norek').val(obj.norek);
                  $('#status').val(obj.status);
                  $('#pangkatcpns').val(obj.pangkatcpns);
                  $('#tmtcpns').val(obj.tmtcpns);
                  $('#skcpns').val(obj.skcpns);
                  $('#skpns').val(obj.skpns);
                  $('#karpeg').val(obj.karpeg);
                  $('#kelasjabatan').val(obj.kelasjabatan);
                  $('#jabatan').val(obj.jabatan);
                  $('#un4').val(obj.u4);
                  $('#un3').val(obj.u3);
                  $('#un2').val(obj.u2);
                  $('#pangkat').val(obj.pangkat);
                  $('#tmt').val(obj.tmt);
                  $('#pendidikan').val(obj.pendidikan);
                  $('#jurusan').val(obj.jurusan);
                  $('#thn_lulus').val(obj.thn_lulus);
                    $('#success-modal').modal('show');
                }
            });
      }
    });
  </script>
</body>
</html>