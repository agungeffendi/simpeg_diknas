<footer class="main-footer d-flex p-2 px-3 bg-white border-top">
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Services</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Products</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
              </li>
            </ul>
            <span class="copyright ml-auto my-auto mr-2">Copyright © 2018
              <a href="https://designrevision.com" rel="nofollow">DesignRevision</a>
            </span>
          </footer>
        </main>
      </div>
    </div>
    <div class="promo-popup animated">
      <a href="http://bit.ly/shards-dashboard-pro" class="pp-cta extra-action">
        <img src="https://dgc2qnsehk7ta.cloudfront.net/uploads/sd-blog-promo-2.jpg"> </a>
      <div class="pp-intro-bar"> Info Terbaru Untuk Anda
        <span class="close">
          <i class="material-icons">close</i>
        </span>
        <span class="up">
          <i class="material-icons">keyboard_arrow_up</i>
        </span>
      </div>
      <div class="pp-inner-content">
        <h2>Gaji Dan Pensiunan</h2>
        <p>Anda akan pensiun dalam 3 Tahun 5 bulan<br>Kenaikan gaji anda dalam 2 tahun 2 bulan.</p>
        <a class="pp-cta extra-action" href="http://bit.ly/shards-dashboard-pro">Download</a>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    

    
    
    <script src="<?php echo base_url(); ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/xlsx/xlsx.core.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/FileSaver/FileSaver.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/TableExport/dist/js/tableexport.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dtable/datatables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/formToWizard/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/formToWizard/jquery.formtowizard.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/formToWizard/jquery.validationEngine.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/formToWizard/jquery.validationEngine-en.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/select2/dist/js/select2.min.js"></script>
    

    <script type="text/javascript">
      $(document).ready(function() {
      $('.pilihan').select2();
      $('.datepicker').datepicker();
      $('.tbl-data').DataTable();
    });
    </script>
    <script>
        $( function() {
            var $signupForm = $( '#SignupForm' );
            
            $signupForm.validationEngine();
            
            $signupForm.formToWizard({
                submitButton: 'SaveAccount',
                showProgress: true, //default value for showProgress is also true
                nextBtnName: 'Tahap Selanjutnya >>',
                prevBtnName: '<< Tahap Sebelumnya',
                showStepNo: false,
                validateBeforeNext: function() {
                    return $signupForm.validationEngine( 'validate' );
                }
            });
            
            
            $( '#txt_stepNo' ).change( function() {
                $signupForm.formToWizard( 'GotoStep', $( this ).val() );
            });
            
            $( '#btn_next' ).click( function() {
                $signupForm.formToWizard( 'NextStep' );
            });
            
            $( '#btn_prev' ).click( function() {
                $signupForm.formToWizard( 'PreviousStep' );
            });
        });
        $(".report").tableExport({
            headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
            footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
            formats: ["xlsx", "xls"],    // (String[]), filetypes for the export
            fileName: "id",                    // (id, String), filename for the downloaded file
            bootstrap: true,                   // (Boolean), style buttons using bootstrap
            position: "top",                 // (top, bottom), position of the caption element relative to table
            ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file(s)
            ignoreCols: null,                  // (Number, Number[]), column indices to exclude from the exported file(s)
            ignoreCSS: ".tableexport-ignore",  // (selector, selector[]), selector(s) to exclude from the exported file(s)
            emptyCSS: ".tableexport-empty",    // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s)
            trimWhitespace: false              // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)
        });
    </script>
  </body>
</html>