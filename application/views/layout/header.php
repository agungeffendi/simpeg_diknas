<?php if (empty($this->session->userdata('nama'))) {
  redirect('/auth', 'refresh');
} ?>
<html>
<head>
    <!-- Basic Page Info -->
  <meta charset="utf-8">
  <title><?php echo ucfirst($this->uri->segment(1)); ?></title>

  <!-- Site favicon -->
  <!-- <link rel="shortcut icon" href="images/favicon.ico"> -->

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>deskapp/vendors/styles/style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>deskapp/src/plugins/jquery-steps/build/jquery.steps.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>deskapp/src/plugins/datatables/media/css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>deskapp/src/plugins/datatables/media/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>deskapp/src/plugins/datatables/media/css/responsive.dataTables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>deskapp/src/plugins/dropzone/src/dropzone.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/combo-tree/style.css">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
  <script src="<?php echo base_url(); ?>deskApp/vendors/scripts/script.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/jQuery-Knob-master/js/jquery.knob.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/highcharts-6.0.7/code/highcharts.js"></script>
  <script src="<?php echo base_url(); ?>deskApp/src/plugins/highcharts-6.0.7/code/highcharts-more.js"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-119386393-1');
  </script>

</head>
<body>
  <div class="pre-loader"></div>
  <div class="header clearfix">
    <div class="header-right">
      <div class="brand-logo">
        <a href="<?php echo base_url(); ?>dashboard/">
          <img src="<?php echo base_url(); ?>assets/logo.png" alt="" class="mobile-logo">
        </a>
      </div>
      <div class="menu-icon">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="user-info-dropdown">
        <div class="dropdown">
          <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
            <span class="user-name"><?php echo $this->session->userdata('nama'); ?></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
            <a class="dropdown-item" href="<?php echo base_url(); ?>auth/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
          </div>
        </div>
      </div>
      <div class="user-notification">
        <div class="dropdown">
          <a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
            <i class="fa fa-bell" aria-hidden="true"></i>
            <span class="badge notification-active"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="notification-list mx-h-350 customscroll">
              <ul>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <img src="<?php echo base_url(); ?>deskapp/vendors/images/img.jpg" alt="">
                    <h3 class="clearfix">John Doe <span>3 mins ago</span></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Side bar is here bruuuh -->
  <div class="left-side-bar">
    <div class="brand-logo">
      <a href="index.php">
        <img src="<?php echo base_url(); ?>assets/logo.png" alt="">
      </a>
    </div>
    <div class="menu-block customscroll">
      <div class="sidebar-menu">
        <ul id="accordion-menu">
          <li>
            <a href="<?php echo base_url(); ?>dashboard/" class="dropdown-toggle no-arrow">
              <span class="fa fa-home"></span><span class="mtext">Dashboard</span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>pegawai/" class="dropdown-toggle no-arrow">
              <span class="fa fa-user"></span><span class="mtext">Data Pegawai</span>
            </a>
          </li>
          <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle">
              <span class="fa fa-pencil"></span><span class="mtext">Master Data</span>
            </a>
            <ul class="submenu">
              <li>
                <a href="<?php echo base_url(); ?>bank">Bank</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>dikstruk">Diklat Struktural</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>diktek">Diklat Teknis</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>hukuman">Hukuman</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>jabatan">Jabatan</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>jabfung">Jabatan Fungsional</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>jurusan">Jurusan</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>pendidikan">Pendidikan</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>award">Penghargaan</a>
              </li>
              <li>
                <a href="<?php echo base_url(); ?>unit">Unit Kerja</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="<?php echo base_url(); ?>user/" class="dropdown-toggle no-arrow">
              <span class="fa fa-users"></span><span class="mtext">Management Users</span>
            </a>
          </li>
          <li>
            <a href="#" class="dropdown-toggle no-arrow">
              <span class="fa fa-book"></span><span class="mtext">Laporan</span>
            </a>
          </li>
          <!-- <li>
            <a href="#" class="dropdown-toggle no-arrow">
              <span class="fa fa-book"></span><span class="mtext">Perizinan</span>
            </a>
          </li> -->
          <li>
            <a href="<?php echo base_url(); ?>auth/logout" class="dropdown-toggle no-arrow">
              <span class="fa fa-sign-out"></span><span class="mtext">Keluar</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="main-container">
    <div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
      <div class="min-height-200px">
        <div class="page-header">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="title">
                <h4><?php echo ucfirst($this->uri->segment(1)); ?></h4>
              </div>
              <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?php echo ucfirst($this->uri->segment(1)); /*if (empty($url)) {}else{ echo $url; }*/ ?></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>