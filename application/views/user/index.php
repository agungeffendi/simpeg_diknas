<div class="card card-statistics">
        <div class="card-body">
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit User</span></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php echo form_open('user/update'); ?>
                  <input type="hidden" name="uid" class="form-control" id="uid">
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="pbaru">Username</label>
                      <input type="text" class="form-control" id="username" disabled> </div>
                    <div class="form-group col-md-6">
                      <label for="pbaru">Password Baru</label>
                      <input type="password" class="form-control" name="password" id="pbaru" placeholder="Password Baru"> </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="feEmailAddress">Email</label>
                      <input type="email" name="mail" class="form-control" id="feEmailAddress" placeholder="Email"> </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                </div>
              </form>
              </div>
            </div>
          </div>
            <h4 class="card-title">Data User</h4>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
            <hr>
            <div id="master_collapse" class="collapse">
              <?php echo validation_errors(); ?>
              <?php echo form_open('user/create'); ?>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input name="mail" type="text" class="form-control" placeholder="Email Aktif" style="width: 50%;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                      <input name="username" type="text" class="form-control" placeholder="Username" style="width: 50%;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lingkup Kerja</label>
                    <div class="col-sm-10">
                      <select name="lingkup" class="pilihan form-control" style="width: 100%;">
                        <option value="">Pilih</option>
                        <?php foreach ($unit as $unt) { ?>
                        <option value="<?php echo $unt['id']; ?>"><?php echo $unt["name"]; ?></option>
                        <?php } ?>
                    </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input name="password" type="password" class="form-control" placeholder="Kombinasi Unik" style="width: 50%;">
                    </div>
                  </div>
                  <button type="submit" name="submit" class="btn btn-inverse-info btn-fw">Simpan</button>
                </form>
            </div><hr>

            <div class="table-responsive">
            <table class="tbl-data table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Eselon Unit</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach ($user as $usr): ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td>
                          <button type="button" class="btn btn-warning" id="aj_call" data-toggle="modal" data-id="<?php echo $usr['username']; ?>" data-target="#exampleModal">
                            Edit
                          </button>
                        <?php echo form_open('user/delete'); ?>
                        <input type="hidden" name="uid" value="<?php echo $usr['id']; ?>">
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus Data?')">Hapus</button>
                        </form>
                        </td>
                        <td><?php echo $usr['username']; ?></td>
                        <td><?php echo $usr['email']; ?></td>
                        <td><?php echo $usr['i_unit']; ?></td>
                        <td><?php echo $usr['flags']; ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            </div>
        </div>
      </div>
      <script type='text/javascript'>
        $(document).on("click","#aj_call", function () {
          var username = $(this).data('id');
          $.ajax({
           url:'<?php echo base_url(); ?>User/detail',
           method: 'post',
           data: {username: username},
           dataType: 'json',
           success: function(response){
            var len = response.length;
            if(len > 0){
             // Read values
             var uname = response[0].username;
             var email = response[0].email;
             var uid = response[0].id;

             $('#feEmailAddress').val(email);
             $('#username').val(username);
             $('#uid').val(uid);
       
            }else{
             console.log('Terjadi Kesalahan');
            }
           }
         });
        });
       </script>