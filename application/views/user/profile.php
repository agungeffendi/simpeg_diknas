<div class="main-content-container container-fluid px-4">
  <!-- Page Header -->
  <div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
      <span class="text-uppercase page-subtitle">Overview</span>
      <h3 class="page-title">Profile Pengguna</h3>
    </div>
  </div>
  <!-- End Page Header -->
  <!-- Default Light Table -->
  <div class="row">
    <div class="col-lg-4">
      <div class="card card-small mb-4 pt-3">
        <div class="card-header border-bottom text-center">
          <div class="mb-3 mx-auto">
            <img class="rounded-circle" src="<?php echo base_url(); ?>uploads/profile/def.png" alt="User Avatar" width="110"> </div>
          <h4 class="mb-0"><?php echo $this->session->userdata['nama']; ?></h4>
          <span class="text-muted d-block mb-2"><?php echo $this->session->userdata['mail']; ?></span>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item p-4">
            <strong class="text-muted d-block mb-2">Description</strong>
            <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</span>
          </li>
        </ul>
      </div>
    </div>
    <div class="col-lg-8">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0">Account Details</h6>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item p-3">
            <div class="row">
              <div class="col">
                <form>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="feFirstName">Username</label>
                      <input type="text" class="form-control" id="feFirstName" placeholder="Username" value="<?php echo $this->session->userdata['nama']; ?>"> </div>
                    <div class="form-group col-md-6">
                      <label for="plama">Password</label>
                      <input type="password" class="form-control" id="plama" placeholder="Password Lama"> </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="feEmailAddress">Email</label>
                      <input type="email" class="form-control" id="feEmailAddress" placeholder="Email" value="<?php echo $this->session->userdata['mail'] ?>"> </div>
                      <div class="form-group col-md-6">
                      <label for="pbaru">Password Baru</label>
                      <input type="password" class="form-control" id="pbaru" placeholder="Password Baru"> </div>
                  </div>
                  <button type="submit" class="btn btn-accent">Perbarui Profil</button>
                </form>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- End Default Light Table -->
</div>