<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
        	<h4 class="card-title">Daftar Jenis Jabatan Fungsi</h4>
        	<div class="row">
        		<a href="<?php echo base_url(); ?>jabfung/create" class="btn btn-inverse-primary btn-fw">Tambah Jenis Jabatan Fungsi</a>
        	</div>
        	<div class="table-responsive">
        	<table class="tbl-data table table-hover table-bordered">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th></th>
        				<th>Nama Jabatan Fungsi</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php $no=1; foreach ($jabfung as $jab): ?>
        			<tr>
        				<td><?php echo $no++; ?></td>
        				<td><a href="<?php echo base_url(); ?>jabfung/edit" class="btn btn-inverse-warning btn-fw">Edit</a></td>
        				<td><?php echo $jab['n_jabfung']; ?></td>
        			</tr>
        			<?php endforeach ?>
        		</tbody>
        	</table>
        	</div>
        </div>
      </div>
    </div>
</div>