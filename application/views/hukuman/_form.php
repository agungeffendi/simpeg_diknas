<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
        	<h4 class="card-title"><?php echo $title; ?></h4>
          <form action="#" method="post">
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nama Jenis Hukuman</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" placeholder="Nama Jenis Diklat Teknis" style="width: 50%;">
              </div>
          </div>
          <div class="form-group row">
                <label class="col-sm-3 col-form-label">Jenis Hukuman</label>
                  <div class="col-sm-9">
                    <select class="form-control" style="width: 30%;">
                        <option value="">Pilih</option>
                        <option value="Ringan">Ringan</option>
                        <option value="Sedang">Sedang</option>
                        <option value="Berat">Berat</option>
                    </select>
                  </div>
            </div>
          <button class="btn btn-default" type="submit">Tambahkan</button>
        </form>
        </div>
      </div>
    </div>
</div>