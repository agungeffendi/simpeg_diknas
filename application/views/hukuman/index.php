<div class="card card-statistics">
    <div class="card-body">
        <h4 class="card-title">Daftar Hukuman</h4>
        <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
        <hr>
        <div id="master_collapse" class="collapse">
          <form action="#" method="post">
          <div class="form-group row">
              <label class="col-sm-2 col-form-label">Nama Jenis Hukuman</label>
              <div class="col-sm-10">
                <input name="hukuman_nama" type="text" class="form-control" placeholder="Nama Hukuman" style="width: 30%;">
              </div>
          </div>
          <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jenis Hukuman</label>
                  <div class="col-sm-10">
                    <select name="hukuman_jenis" class="form-control" style="width: 30%;">
                        <option value="">Pilih</option>
                        <option value="Ringan">Ringan</option>
                        <option value="Sedang">Sedang</option>
                        <option value="Berat">Berat</option>
                    </select>
                  </div>
            </div>
          <button class="btn btn-default" type="submit">Tambahkan</button>
        </form>
        </div><hr>
        <div class="table-responsive">
        <table class="tbl-data table table-hover table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th></th>
                    <th>Nama Hukuman</th>
                    <th>Janis Hukuman</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach ($hukuman as $hkm): ?>
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td></td>
                    <td><?php echo $hkm['n_hukuman']; ?></td>
                    <td><?php echo $hkm['c_hukuman_level']; ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        </div>
    </div>
</div>