<div class="row">
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
        	<h4 class="card-title">Daftar Award</h4>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#master_collapse">Tambah</button>
            <hr>
            <div id="master_collapse" class="collapse">
              <form action="#" method="post">
                  <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Nama Penghargaan</label>
                      <div class="col-sm-10">
                        <input name="penghargaan_nama" type="text" class="form-control" placeholder="Penghargaan" style="width: 50%;">
                      </div>
                  </div>
                  <button class="btn btn-default" type="submit">Tambahkan</button>
                </form>
            </div><hr>
        	<div class="table-responsive">
        	<table class="tbl-data table table-hover table-bordered">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th></th>
        				<th>Nama Penghargaan</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php $no=1; foreach ($award as $award): ?>
        			<tr>
        				<td><?php echo $no++; ?></td>
        				<td></td>
        				<td><?php echo $award['n_penghargaan']; ?></td>
        			</tr>
        			<?php endforeach ?>
        		</tbody>
        	</table>
        	</div>
        </div>
      </div>
    </div>
</div>